cmake_minimum_required(VERSION 3.0)
project(ld23)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/lib/benzaiten/cmake)
include(SetVersion)
SET_VERSION(1 0 0)

add_subdirectory(assets)
add_subdirectory(lib)
add_subdirectory(src)

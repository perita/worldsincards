#!/bin/sh
set -e

case `uname -s` in
    *MINGW*)
        BUILD_DIR=mingw
        GENERATOR="MSYS Makefiles"
        ;;
    *)
        BUILD_DIR=unix
        GENERATOR="Unix Makefiles"
        ;;
esac

rm -fr build/${BUILD_DIR}
mkdir -p build/${BUILD_DIR}
cd build/${BUILD_DIR}
cmake -DCMAKE_CXX_FLAGS="-Wall" -DCMAKE_C_FLAGS="-Wall" -DCMAKE_OSX_ARCHITECTURES="i386;ppc" -DCMAKE_BUILD_TYPE="Release" -G "${GENERATOR}" ../..
make

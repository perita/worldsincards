set (MACOSX_BUNDLE_INFO_STRING "Ludum Dare 23 Entry")
set (MACOSX_BUNDLE_GUI_IDENTIFIER "com.geishastudios.ld23")
set (MACOSX_BUNDLE_LONG_VERSION_STRING "${VERSION}")
set (MACOSX_BUNDLE_BUNDLE_NAME "worldsincards")
set (MACOSX_BUNDLE_SHORT_VERSION_STRING "${VERSION}")
set (MACOSX_BUNDLE_BUNDLE_VERSION "${VERSION_REV}")
set (MACOSX_BUNDLE_COPYRIGHT "Copyright (c) 2012 Geisha Studios")

set(ld23_HEADERS
    File.hpp
    Font.hpp
    String.hpp
    contexts/Level.hpp
    contexts/LoadLevel.hpp
    contexts/SelectLevel.hpp
    sprites/Card.hpp
    sprites/Critter.hpp
    sprites/Goal.hpp
    sprites/Label.hpp
    sprites/Lava.hpp
    sprites/LevelOption.hpp
    sprites/Player.hpp
    sprites/Tenant.hpp
    sprites/Walker.hpp
    )

set(ld23_SOURCES
    main.cpp
    File.cpp
    Font.cpp
    String.cpp
    contexts/Level.cpp
    contexts/LoadLevel.cpp
    contexts/SelectLevel.cpp
    sprites/Card.cpp
    sprites/Critter.cpp
    sprites/Goal.cpp
    sprites/Label.cpp
    sprites/Lava.cpp
    sprites/LevelOption.cpp
    sprites/Player.cpp
    sprites/Tenant.cpp
    sprites/Walker.cpp
    )

include(UseBenzaiten)
ADD_WIN32_RESOURCE(ld23.rc ld23_SOURCES)
USE_CONFIG_H()
BENZAITEN_GAME(worldsincards "${ld23_HEADERS}" "${ld23_SOURCES}")

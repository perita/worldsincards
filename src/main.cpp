//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include <cstdlib>
#include <string>
#include <benzaiten/ErrorMessage.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include <benzaiten/contexts/Logo.hpp>
#include "contexts/LoadLevel.hpp"
#include "contexts/SelectLevel.hpp"

namespace ld23 {
    class Main: public benzaiten::Game {
        public:
            Main (const std::string &level_file_name)
                : Game (400, 240, "Worlds in Cards", "worldsincards")
                , level_file_name (level_file_name)
            {
                this->setScreenFillPolicy (benzaiten::Screen::FILL_BOTH);
            }

            virtual void init () {
                using namespace benzaiten;
                Game::screen ().color = benzaiten::makeColor (155, 188, 15);
                Game::input ().mapKeys (
                        FWD_ACTION, std::vector<int> (1, SDLK_SPACE));
                if (this->level_file_name.empty ()) {
                    this->switchTo (SelectLevel::New ());
                } else {
                    this->switchTo (LoadLevel::New (0, this->level_file_name));
                }
            }

        private:
            std::string level_file_name;
    };
}

int main (int argc, char *argv[]) {
    try {
        std::string file_name;
#if !defined (APPLE)
        if (argc > 1) {
            file_name = argv[1];
        }
#endif // !APPLE

        ld23::Main game (file_name);
        game ();
        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        benzaiten::showErrorMessage (e.what ());
    } catch (...) {
        benzaiten::showUnknownErrorMessage ();
    }
    return EXIT_FAILURE;
}

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "SelectLevel.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/contexts/FadeTo.hpp>
#include <benzaiten/SpriteGroup.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include "LoadLevel.hpp"
#include "../File.hpp"
#include "../Font.hpp"

using namespace benzaiten;
using namespace ld23;

namespace {
    const int OPTIONS_SIZE = 64;
    const int OPTIONS_SPACING = OPTIONS_SIZE + 10;
    const int OPTIONS_PER_ROW = 5;
    const int OPTIONS_START_X = 400 / 2 -
        (OPTIONS_SPACING * (OPTIONS_PER_ROW - 1)) / 2;
    const int OPTIONS_START_Y = 115;
    const int TITLE_Y = 30;
    const double TWEENERS_TIME = 0.50;
    const double TWEENERS_DELAY = 0.10;
}

SelectLevel::SelectLevel ()
    : Context ()
    , levels ()
    , music (benzaiten::Game::resources ().try_music ("bgm.ogg"))
{
    File file (Game::resources ().getFilePath ("stages", "all.txt"));
    std::string level_file;
    while (file.readLine (level_file)) {
        this->levels.push_back (
                Game::resources ().getFilePath ("stages", level_file));
    }
    assert (!this->levels.empty () && "There are no levels to select from");

    Texture::Ptr title =
        Texture::New (Game::resources ().texture ("selectlevel.tga"));
    title->centerOrigin ();
    this->addGraphic (title, Game::screen ().half_width, TITLE_Y);

    BitmapFont font (font::bold ());
    int x = OPTIONS_START_X;
    int y = OPTIONS_START_Y;
    int row = 0;
    for (unsigned int level = 0; level < this->levels.size (); ++level) {
        SpriteGroup::Ptr option =
            SpriteGroup::New (x, y + Game::screen ().height);
        option->add (LevelOption::New (*this, level, 0, 0));
        Text::Ptr text = Text::New (font,
                (boost::format ("%|02|") % (level + 1)).str ());
        text->centerOrigin ();
        option->add (
                Sprite::New (0, OPTIONS_SIZE / 2 + font.height () / 2, text))->
            setActive (false);
        this->add (option);

        this->addTweener (tweener::Simple::New (option->y (), y, TWEENERS_TIME,
                    [option](double y) { option->moveToY(y); },
                    easing::Linear::easeInOut))->
            setDelay (TWEENERS_DELAY * row);

        if ((level + 1) % OPTIONS_PER_ROW == 0) {
            ++row;
            y += OPTIONS_SPACING + font.height () + 10;
            x = OPTIONS_START_X;
        } else {
            x += OPTIONS_SPACING;
        }
    }

    this->music->play ();
}

void SelectLevel::selected (LevelOption &level) {
    int level_index = level.getLevel ();
    Game::switchTo (contexts::FadeTo::New (
                LoadLevel::New (level_index + 1,
                    this->levels[level_index], Game::activeContext (),
                    boost::bind (&LevelOption::markAsCompleted, &level))));
}

void SelectLevel::update (double elapsed) {
    if (Game::input ().pressed (Action::MENU_CANCEL)) {
        Game::quit ();
    } else {
        Context::update (elapsed);
    }
}

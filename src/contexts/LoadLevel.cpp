//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "LoadLevel.hpp"
#include <algorithm>
#include <sstream>
#include <stdexcept>
#include <benzaiten/Game.hpp>
#include <boost/format.hpp>
#include "../File.hpp"
#include "../String.hpp"
#include "../sprites/Critter.hpp"
#include "../sprites/Goal.hpp"
#include "../sprites/Lava.hpp"
#include "../sprites/Player.hpp"

using namespace benzaiten;
using namespace ld23;

namespace {
    void check_stream (const std::istream &stream, const std::string &what,
            int line_num) {
        if (!stream) {
            throw std::runtime_error (
                    (boost::format ("Couldn't read %1% at line %2%")
                        % what % line_num).str ());
        }
    }

    void check_constraint (bool satisfied, const std::string &what,
            int line_num) {
        if (!satisfied) {
            throw std::runtime_error (
                    (boost::format ("Constraint not satisfied at %1%: %2%")
                        % line_num % what).str ());
        }
    }

    void check_constraint (bool satisfied, const std::string &what,
            const std::string &file_name) {
        if (!satisfied) {
            throw std::runtime_error (
                    (boost::format ("Constraint not satisfied in %1%: %2%")
                        % file_name % what).str ());
        }
    }

}

LoadLevel::LoadLevel (int level_num, const std::string &file_name,
        const Context::Ptr &next, Level::Callback callback)
    : Context ()
    , callback (callback)
    , cards ()
    , file_name (file_name)
    , level_num (level_num)
    , max_column (0)
    , max_row (0)
    , next (next)
    , seen_goal (false)
    , seen_player (false)
    , tenants ()
    , texts ()
{
}

void LoadLevel::addTenant (int row, int column, const Tenant::Ptr &tenant) {
    this->tenants.push_back (Level::TenantDef (row, column, tenant));
}

void LoadLevel::eatChar (std::istream &input) {
    char nom;
    input >> nom;
}

void LoadLevel::begin () {
    File file (this->file_name);
    std::string line;
    int line_num = 1;
    while (file.readLine (line)) {
        std::istringstream input (line);

        char command;
        check_stream (input >> command, "command", line_num);
        switch (command) {
            // Comment
            case '#':
                // Nothing to do
                break;

            // Card
            case 'c':
                this->parseCard (input, line_num);
                break;

            // Text
            case 't':
                this->parseText (input, line_num);
                break;

            // Oops
            default:
                throw std::runtime_error (
                        (boost::format ("Command `%1%' is not valid at line %2%")
                            % line[0] % line_num).str ());
        }
        ++line_num;
    }
    check_constraint (!this->cards.empty (), "no cards defined", file_name);
    check_constraint (!this->tenants.empty (), "no tenants defined", file_name);
    check_constraint (this->seen_goal, "no goals defined", file_name);
    check_constraint (this->seen_player, "no player position defined",
            file_name);

    Game::switchTo (Level::New (this->level_num, this->max_row + 1,
                this->max_column + 1, this->cards, this->tenants,
                this->texts, this->next, this->callback));
}

void LoadLevel::parseCard (std::istream &input, int line_num) {
    int type;
    check_stream (input >> type, "card type", line_num);
    check_constraint (type >= 0, "card type can't be negative", line_num);

    int column;
    check_stream (input >> column, "card column", line_num);
    check_constraint (column >= 0, "card column can't be negative", line_num);
    this->max_column = std::max (this->max_column, column);

    int row;
    check_stream (input >> row, "card row", line_num);
    check_constraint (row >= 0, "card row can't be negative", line_num);
    this->max_row = std::max (this->max_row, row);

    this->cards.push_back (Level::CardDef (type, row, column));

    while (!input.eof ()) {
        char extra;
        check_stream (input >> extra, "card extra type", line_num);
        switch (extra) {
            // Goal
            case '<':
                this->parseGoal (input, row, column, line_num);
                break;

                // Player
            case '@':
                this->addTenant (row, column, Player::New ());
                this->seen_player = true;
                this->eatChar (input);
                break;

                // Critter
            case 'e':
                this->parseCritter (input, row, column, line_num);
                break;

                // Lava
            case 'l':
                this->addTenant (row, column, Lava::New ());
                this->eatChar (input);
                break;

                // Oops
            default:
                throw std::runtime_error (
                        (boost::format ("Invalid extra card param `%1%' at line %2%")
                         % extra % line_num).str ());
                break;
        }
    }
}

void LoadLevel::parseCritter (std::istream &input, int row, int column,
        int line_num) {
    char dir;
    check_stream (input >> dir, "critter direction", line_num);
    check_constraint (dir == '+' || dir == '-',
            "critter direction is neither left (-) nor right (+)", line_num);

    double speed;
    check_stream (input >> speed, "critter speed", line_num);
    check_constraint (speed > 0.0, "the critter's speed can't be 0 nor negative",
            line_num);

    this->addTenant (row, column, Critter::New (dir == '+' ? 1 : -1, speed));
}

void LoadLevel::parseGoal (std::istream &input, int row, int column,
        int line_num) {
    int x;
    check_stream (input >> x, "goal x", line_num);
    check_constraint (x >= 0, "goal x can't be negative", line_num);
    check_constraint (x < CARD_WIDTH, "goal x can't be greater than card's width",
            line_num);

    int y;
    check_stream (input >> y, "goal y", line_num);
    check_constraint (y >= 0, "goal y can't be negative", line_num);
    check_constraint (y < CARD_HEIGHT, "goal y can't be greater than card's height",
            line_num);

    this->addTenant (row, column, Goal::New (x, y));
    this->seen_goal = true;
}

void LoadLevel::parseText (std::istream &input, int line_num) {
    int x;
    check_stream (input >> x, "text x", line_num);

    int y;
    check_stream (input >> y, "text y", line_num);

    std::string value;
    std::getline (input, value);
    check_stream (input, "text value", line_num);
    trim (value);
    check_constraint (!value.empty (), "the text can't be empty", line_num);

    this->texts.push_back (Level::Text (x, y, value));
}

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_CONTEXTS_LEVEL_HPP)
#define GEISHA_STUDIOS_LD23_CONTEXTS_LEVEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <vector>
#include <benzaiten/Context.hpp>
#include <benzaiten/Point2D.hpp>
#include <benzaiten/Sound.hpp>
#include <benzaiten/Timer.hpp>
#include <boost/function.hpp>
#include <boost/multi_array.hpp>
#include <boost/shared_ptr.hpp>
#include "../sprites/Card.hpp"
#include "../sprites/Goal.hpp"
#include "../sprites/Tenant.hpp"
#include "../sprites/Player.hpp"

namespace ld23 {
    class Level: public benzaiten::Context, Goal::Observer, Player::Observer {
        public:
            typedef boost::function<void (void)> Callback;
            typedef boost::shared_ptr<Level> Ptr;

            struct CardDef {
                int column;
                int row;
                int type;

                CardDef (int type, int row, int column)
                    : column (column)
                    , row (row)
                    , type (type)
                {
                }
            };

            struct TenantDef {
                int column;
                int row;
                Tenant::Ptr object;

                TenantDef (int row, int column, const Tenant::Ptr &object)
                    : column (column)
                    , row (row)
                    , object (object)
                {
                }
            };

            struct Text {
                const std::string value;
                int x;
                int y;

                Text (int x, int y, const std::string &value)
                    : value (value)
                    , x (x)
                    , y (y)
                {
                }
            };

            typedef std::vector<CardDef> CardsDef;
            typedef std::vector<TenantDef> TenantsDef;
            typedef std::list<Text> Texts;

            static Ptr New (int level_num, int rows, int columns,
                    const CardsDef &cards_def, const TenantsDef &tenants,
                    const Texts &texts, const Context::Ptr &next,
                    Callback callback) {
                return Ptr (new Level (level_num, rows, columns, cards_def,
                            tenants, texts, next, callback));
            }

            virtual void begin ();
            virtual void dead (Player &player);
            virtual void end ();
            virtual void removed (Goal &goal);
            virtual void update (double elapsed);

        protected:
            Level (int level_num, int rows, int columns,
                    const CardsDef &cards_def, const TenantsDef &tenants,
                    const Texts &texts, const Context::Ptr &next,
                    Callback callback);

        private:
            Card::Ptr getCardAt (int x, int y) const;
            int getCardColumn (int x) const;
            int getCardRow (int y) const;
            int getCardX (int column) const;
            int getCardY (int row) const;
            int getRows () const;
            int getColumns () const;
            void moveCardTo (Card &card, int row, int column);
            void resetSelectedCard ();
            void selectCard (const Card::Ptr &card);
            void setCompleted ();
            void setupCards ();
            void showNextText ();
            void swapSelectedCardWith (Card &card);
            bool toggleSelected (benzaiten::Timer &timer);
            void updateNeighbors (const Card::Ptr &one, const Card::Ptr &other,
                    Card::Neighbor which_one, Card::Neighbor which_other);
            Card::Ptr updateNeighbors (int row, int column);

            benzaiten::Timer::Ptr blink;
            Callback callback;
            boost::multi_array<Card::Ptr, 2> cards;
            benzaiten::Point2D<int> cards_pos;
            bool completed;
            benzaiten::Sound::Ptr goal_sfx;
            unsigned int goals;
            Context::Ptr next;
            Card::Ptr selected;
            Texts texts;
            benzaiten::Sound::Ptr whoosh;
    };
}

#endif // !GEISHA_STUDIOS_LD23_CONTEXTS_LEVEL_HPP


//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Level.hpp"
#include <algorithm>
#include <cassert>
#include <benzaiten/Game.hpp>
#include <benzaiten/Utils.hpp>
#include <benzaiten/VirtualJoystick.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include <benzaiten/graphics/FillColor.hpp>
#include <benzaiten/tweeners/Group.hpp>
#include <benzaiten/tweeners/Sequence.hpp>
#include <benzaiten/tweeners/Simple.hpp>
#include <benzaiten/tweeners/easing/Linear.hpp>
#include <benzaiten/tweeners/easing/Bounce.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include "../sprites/Label.hpp"
#include "../Font.hpp"

using namespace benzaiten;
using namespace ld23;

namespace {
    const double BLINK_PERIOD = 0.1;
    const double LEVEL_TEXT_X = 15;
    const double LEVEL_TEXT_Y = 20;
    const double SWAP_TIME = 0.1;
}

Level::Level (int level_num, int rows, int columns, const CardsDef &cards_def,
                    const TenantsDef &tenants, const Texts &texts,
                    const Context::Ptr &next, Callback callback)
    : Context ()
    , blink (Timer::New (BLINK_PERIOD,
                boost::bind (&Level::toggleSelected, this, _1)))
    , callback (callback)
    , cards (boost::extents[rows][columns])
    , cards_pos (
            Game::screen ().half_width - (columns * (CARD_WIDTH - 1) / 2),
            Game::screen ().half_height - (rows * (CARD_HEIGHT - 1) / 2))
    , completed (false)
    , goal_sfx (Game::resources ().try_sound ("goal.ogg"))
    , goals (0)
    , next (next)
    , selected ()
    , texts (texts)
    , whoosh (Game::resources ().try_sound ("whoosh.ogg"))
{
    this->addTimer (this->blink);
    assert (!cards_def.empty () && "Can't have a level without cards");
    assert (!tenants.empty () && "Can't have a level without tenants");

    BOOST_FOREACH (const CardDef &card_def, cards_def) {
        assert (!this->cards[card_def.row][card_def.column] &&
                "There is another card in the same position of this level");
        this->cards[card_def.row][card_def.column] = Card::New (card_def.type);
    }
    this->setupCards ();
    BOOST_FOREACH (const TenantDef &tenant, tenants) {
        tenant.object->setFirstCard (this->cards[tenant.row][tenant.column]);
        this->add (tenant.object);
        switch (tenant.object->getType ()) {
            case Types::GOAL:
                static_cast<Goal &>(*tenant.object).setObserver (this);
                ++this->goals;
                break;

            case Types::PLAYER:
                static_cast<Player &>(*tenant.object).setObserver (this);
                break;
        }
    }

    benzaiten::Text::Ptr level_text = benzaiten::Text::New (font::bold (),
            (boost::format ("LEVEL %1%") % level_num).str ());
    this->addGraphic (level_text, LEVEL_TEXT_X, LEVEL_TEXT_Y);

    this->showNextText ();
}

void Level::begin () {
    const int size = 32;
    TextureAtlas::Ptr fwd = TextureAtlas::New (
            Game::resources ().texture ("fwd.tga"), size, size);
    VirtualJoystick::Ptr joystick = VirtualJoystick::New (fwd);
    joystick->addButton (FWD_ACTION,
            Game::screen ().width - size / 2 - 5, size / 2 + 5, 0);

    Game::input ().setVirtualJoystick (joystick);
}

void Level::dead (Player &player) {
    if (this->completed) {
        // Don't bother now
        return;
    }

    this->setCompleted ();

    Texture::Ptr success = Texture::New (
            Game::resources ().texture ("oops.tga"));
    success->centerOrigin ();
    Sprite::Ptr spr = this->addGraphic (success,
            Game::screen ().half_width, -30, 3);

    FillColor::Ptr color = FillColor::New (makeColor (48, 98, 48));
    this->addGraphic (color, 0, 0, 3);

    tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
    tweeners->setFinishCallback (boost::bind (&Game::switchTo, this->next));

    tweener::Group::Ptr fade = tweener::Group::New ();
    tweeners->add (fade);
    fade->add (tweener::Simple::New (SDL_ALPHA_TRANSPARENT,
                SDL_ALPHA_OPAQUE, 0.25,
                boost::bind (&FillColor::setAlpha, color.get (), _1),
                easing::Linear::easeInOut));
    fade->add (tweener::Simple::New (-30.0, Game::screen ().half_height, 0.5,
                boost::bind (&Sprite::moveToY, spr.get (), _1),
                easing::Bounce::easeOut));

    tweeners->add (tweener::Simple::New (Game::screen ().half_width,
                -100.0, 0.15,
                boost::bind (&Sprite::moveToX, spr.get (), _1),
                easing::Linear::easeInOut))->setDelay (0.5);

    this->addTweener (tweeners);
}

void Level::end () {
    Game::input ().removeVirtualJoystick ();
}

Card::Ptr Level::getCardAt (int x, int y) const {
    int card_column = this->getCardColumn (x);
    if (card_column < 0 || card_column >= this->getColumns ()) {
        return Card::Ptr ();
    }
    int card_row = this->getCardRow (y);
    if (card_row < 0 || card_row >= this->getRows ()) {
        return Card::Ptr ();
    }

    return this->cards[card_row][card_column];
}

int Level::getCardColumn (int x) const {
    return (x - this->cards_pos.x) / (CARD_WIDTH - 1);
}

int Level::getCardRow (int y) const {
    return (y - this->cards_pos.y) / (CARD_HEIGHT - 1);
}

int Level::getCardX (int column) const {
    // The CARD_WIDTH - 1 is to make the cards to share borders making it
    // better looking and the collisions between cards works without ugly
    // hacks.
    return this->cards_pos.x + column * (CARD_WIDTH - 1);
}

int Level::getCardY (int row) const {
    // See getCardX for the meaning of CARD_HEIGHT - 1
    return this->cards_pos.y + row * (CARD_HEIGHT - 1);
}

int Level::getRows () const {
    return this->cards.shape ()[0];
}

int Level::getColumns () const {
    return this->cards.shape ()[1];
}

void Level::moveCardTo (Card &card, int row, int column) {
    card.moveTo (this->getCardX (column), this->getCardY (row));
}

void Level::removed (Goal &goal) {
    if (this->completed) {
        // Ignore, ignore
        return;
    }

    assert (this->goals > 0 && "Something is weird with the goals");
    if (--this->goals == 0) {
        this->setCompleted ();
        if (this->callback) {
            this->callback ();
        }
        Texture::Ptr success = Texture::New (
                Game::resources ().texture ("success.tga"));
        success->centerOrigin ();
        Sprite::Ptr spr = this->addGraphic (success,
                0, Game::screen ().half_height, 3);


        FillColor::Ptr color = FillColor::New (makeColor (48, 98, 48));
        this->addGraphic (color, 0, 0, 3);

        tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
        tweeners->setFinishCallback (boost::bind (&Game::switchTo, this->next));

        tweener::Group::Ptr fade = tweener::Group::New ();
        tweeners->add (fade);
        fade->add (tweener::Simple::New (SDL_ALPHA_TRANSPARENT,
                    SDL_ALPHA_OPAQUE, 0.5,
                    boost::bind (&FillColor::setAlpha, color.get (), _1),
                    easing::Linear::easeInOut));
        fade->add (tweener::Simple::New (Game::screen ().width + 140,
                    Game::screen ().half_width, 0.5,
                    boost::bind (&Sprite::moveToX, spr.get (), _1),
                    easing::Linear::easeInOut));

        tweeners->add (tweener::Simple::New (Game::screen ().half_height, -25,
                    0.5, boost::bind (&Sprite::moveToY, spr.get (), _1),
                    easing::Linear::easeInOut))->setDelay (0.5);

        this->addTweener (tweeners);
    }
}

void Level::resetSelectedCard () {
    if (this->selected) {
        this->selected->setVisible (true);
        this->selected.reset ();
    }
}

void Level::selectCard (const Card::Ptr &card) {
    this->resetSelectedCard ();
    this->selected = card;
    this->blink->reset ();
}

void Level::setCompleted () {
    this->completed = true;
    Game::input ().removeVirtualJoystick ();
}

void Level::setupCards () {
    int rows = this->getRows ();
    int columns = this->getColumns ();
    for (int row = 0 ; row < rows ; ++row) {
        for (int column = 0 ; column < columns ; ++column) {
            Card::Ptr card = this->updateNeighbors (row, column);
            if (card) {
                this->moveCardTo (*card, row, column);
                this->add (card);
            }
        }
    }
}

void Level::showNextText () {
    Texts::iterator text = this->texts.begin ();
    if (text != this->texts.end ()) {
        this->add (Label::New (text->x, text->y, text->value,
                    boost::bind (&Level::showNextText, this)));
        this->texts.erase (text);
    }
}

void Level::swapSelectedCardWith (Card &card) {
    assert (this->selected && "No selected card to swap");
    assert (this->selected->isEmpty() && card.isEmpty () &&
            "I can only swap empty cards");
    int card_column = this->getCardColumn (card.x ());
    int card_row = this->getCardRow (card.y ());
    int selected_column = this->getCardColumn (this->selected->x ());
    int selected_row = this->getCardRow (this->selected->y ());

    using std::swap;
    swap (this->cards[card_row][card_column],
            this->cards[selected_row][selected_column]);
    this->updateNeighbors (card_row, card_column);
    this->updateNeighbors (selected_row, selected_column);

    // Move the selected.
    this->addTweener (tweener::Simple::New (this->selected->x (),
                this->getCardX (card_column), SWAP_TIME,
                boost::bind (&Card::moveToX, this->selected, _1),
                easing::Linear::easeInOut));
    this->addTweener (tweener::Simple::New (this->selected->y (),
                this->getCardY (card_row), SWAP_TIME,
                boost::bind (&Card::moveToY, this->selected, _1),
                easing::Linear::easeInOut));
    // and move the Card
    this->addTweener (tweener::Simple::New (card.x (),
                this->getCardX (selected_column), SWAP_TIME,
                boost::bind (&Card::moveToX, &card, _1),
                easing::Linear::easeInOut));
    this->addTweener (tweener::Simple::New (card.y (),
                this->getCardY (selected_row), SWAP_TIME,
                boost::bind (&Card::moveToY, &card, _1),
                easing::Linear::easeInOut));

    this->whoosh->play ();
    this->resetSelectedCard ();
}

bool Level::toggleSelected (Timer &timer) {
    if (this->selected) {
        this->selected->setVisible (!this->selected->isVisible ());
    }
    return true;
}

void Level::update (double elapsed) {

    if (Game::input ().pressed (Action::MENU_CANCEL)) {
        Game::switchTo (this->next);
        return;
    } else if (Game::input ().isDown (FWD_ACTION)) {
        elapsed *= 4;
    }

    Context::update (elapsed);

    if (this->completed) {
        return;
    }

    // This could happen if the someone moves to an already selected card.
    if (this->selected && !this->selected->isEmpty ()) {
        this->resetSelectedCard ();
    }

    if (Game::input ().mouse_pressed) {
        Card::Ptr clicked =
            this->getCardAt (Game::input ().mouse_x, Game::input ().mouse_y);
        if (clicked && clicked->isEmpty ()) {
            if (clicked == this->selected) {
                this->resetSelectedCard ();
            } else if (selected) {
                this->swapSelectedCardWith (*clicked);
            } else {
                this->selectCard (clicked);
            }
        }
    }
}

void Level::updateNeighbors (const Card::Ptr &one, const Card::Ptr &other,
        Card::Neighbor which_one, Card::Neighbor which_other) {
    one->setNeighbor (which_one, other);
    if (other) {
        other->setNeighbor (which_other, one);
    }
}

Card::Ptr Level::updateNeighbors (int row, int column) {
    Card::Ptr card = this->cards[row][column];
    if (card) {
        if (row > 0) {
            this->updateNeighbors (card, this->cards[row - 1][column],
                    Card::TOP, Card::BOTTOM);
        } else {
            card->setNeighbor (Card::TOP, Card::Ptr ());
        }
        if (column < this->getColumns () - 1) {
            this->updateNeighbors (card, this->cards[row][column + 1],
                    Card::RIGHT, Card::LEFT);
        } else {
            card->setNeighbor (Card::RIGHT, Card::Ptr ());
        }
        if (row < this->getRows () - 1) {
            this->updateNeighbors (card, this->cards[row + 1][column],
                    Card::BOTTOM, Card::TOP);
        } else {
            card->setNeighbor (Card::BOTTOM, Card::Ptr ());
        }
        if (column > 0) {
            this->updateNeighbors (card, this->cards[row][column - 1],
                    Card::LEFT, Card::RIGHT);
        } else {
            card->setNeighbor (Card::LEFT, Card::Ptr ());
        }
    }
    return card;
}

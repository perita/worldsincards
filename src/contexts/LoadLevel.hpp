//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_CONTEXTS_LOAD_LEVEL_HPP)
#define GEISHA_STUDIOS_LD23_CONTEXTS_LOAD_LEVEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Context.hpp>
#include <boost/shared_ptr.hpp>
#include "Level.hpp"

namespace ld23 {
    class LoadLevel: public benzaiten::Context {
        public:
            typedef boost::shared_ptr<LoadLevel> Ptr;

            static Ptr New (int level_num, const std::string &file_name,
                    const Context::Ptr &next = Context::Ptr (),
                    Level::Callback callback = Level::Callback ()) {
                return Ptr (new LoadLevel (level_num, file_name, next,
                            callback));
            }

            virtual void begin ();

        protected:
            LoadLevel (int level_num, const std::string &file_name,
                    const Context::Ptr &next, Level::Callback callback);

        private:
            void addTenant (int row, int column, const Tenant::Ptr &tenant);
            void eatChar (std::istream &input);
            void parseCard (std::istream &input, int line_num);
            void parseCritter (std::istream &input, int row, int col, int line_num);
            void parseGoal (std::istream &input, int row, int col, int line_num);
            void parseText (std::istream &ibput, int line_num);

            Level::Callback callback;
            Level::CardsDef cards;
            std::string file_name;
            int level_num;
            int max_column;
            int max_row;
            Context::Ptr next;
            bool seen_goal;
            bool seen_player;
            Level::TenantsDef tenants;
            Level::Texts texts;
    };
}

#endif // !GEISHA_STUDIOS_LD23_CONTEXTS_LOAD_LEVEL_HPP

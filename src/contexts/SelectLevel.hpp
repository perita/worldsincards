//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_CONTEXTS_SELECT_LEVEL_HPP)
#define GEISHA_STUDIOS_LD23_CONTEXTS_SELECT_LEVEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <vector>
#include <benzaiten/Context.hpp>
#include <benzaiten/Music.hpp>
#include <boost/shared_ptr.hpp>
#include "../sprites/LevelOption.hpp"

namespace ld23 {
    class SelectLevel: public benzaiten::Context, LevelOption::Observer {
        public:
            typedef boost::shared_ptr<SelectLevel> Ptr;

            static Ptr New () { return Ptr (new SelectLevel ()); }

            virtual void selected (LevelOption &option);
            virtual void update (double elapsed);

        protected:
            SelectLevel ();

        private:
            std::vector<std::string> levels;
            benzaiten::Music::Ptr music;
    };
}

#endif // !GEISHA_STUDIOS_LD23_CONTEXTS_SELECT_LEVEL_HPP

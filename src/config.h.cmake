/*
 * Ludum Dare 23
 * Copyright (C) 2012 Geisha Studios
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef GEISHA_STUDIOS_LD23_CONFIG_H
#define GEISHA_STUDIOS_LD23_CONFIG_H

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif /* _MSC_VER && _MSC_VER >= 1020 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Name of the package. */
#define PACKAGE "@PROJECT_NAME@"

/* Address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "jfita@geishastudios.com"

/* The full name of this package. */
#define PACKAGE_NAME "@MACOSX_BUNDLE_BUNDLE_NAME@"

/* The full name and version of this package. */
#define PACKAGE_STRING "@MACOSX_BUNDLE_BUNDLE_NAME@ @VERSION_MAJOR@.@VERSION_MINOR@.@VERSION_PATCH@"

/* The version of this package. */
#define PACKAGE_VERSION "@VERSION_MAJOR@.@VERSION_MINOR@.@VERSION_PATCH@"

/* The path to the data directory. */
#define PACKAGE_DATA_DIR "@CMAKE_INSTALL_PREFIX@/share"

/* Define to 1 if compiling for Windows. */
#cmakedefine WIN32 1

/* Define to 1 if compiling for Mac OS X. */
#cmakedefine APPLE 1

/* Define to 1 if compiling for GP2X */
#cmakedefine GP2X 1

/* Define to 1 if compiling for A320 */
#cmakedefine A320 1

/* Define to 1 if compiling for an UNIX variant (even Mac OS X.) */
#cmakedefine UNIX 1

/* Define to 1 if compiling for Android. */
#cmakedefine ANDROID 1

/* The height of a card in pixels. */
#define CARD_HEIGHT 32

/* The width of a card in pixels. */
#define CARD_WIDTH 64

/* The gravity force. */
#define GRAVITY 40

/* The forward action. */
#define FWD_ACTION 42

/* The different sprite types. */
struct Types {
    enum {
        FLOOR,
        GOAL,
        HAZARD,
        LEDDER_UP,
        LEDDER_UP_END,
        LEDDER_DOWN,
        LEDDER_DOWN_END,
        PLAYER,
        WALL,
        WALL_CRITTER
    };
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !GEISHA_STUDIOS_LD23_CONFIG_H */

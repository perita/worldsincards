//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_SPRITES_CARD_HPP)
#define GEISHA_STUDIOS_LD23_SPRITES_CARD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/SpriteGroup.hpp>
#include <boost/shared_ptr.hpp>

namespace ld23 {
    class Walker;

    class Card: public benzaiten::SpriteGroup {
        public:
            typedef boost::shared_ptr<Card> Ptr;

            enum Neighbor {
                BOTTOM,
                LEFT,
                RIGHT,
                TOP
            };

            static Ptr New (int type);

            void enter ();
            void checkExits (Walker &walker, int dir_x, int dir_y);
            Card::Ptr getNeighbor (Neighbor which) const;
            bool hasLadderDown () const { return this->ladder_down; }
            bool hasLadderUp () const { return this->ladder_up; }
            bool isEmpty () const { return this->tenants == 0; }
            void leave ();
            void setLadderDown (bool has) { this->ladder_down = true; }
            void setLadderUp (bool has) { this->ladder_up = true; }
            void setNeighbor (Neighbor which, const Card::Ptr &card);

        protected:
            Card ();

        private:
            Sprite::Ptr border[4];
            Card::Ptr neighbor[4];
            unsigned int tenants;
            bool ladder_down;
            bool ladder_up;
    };
}

#endif // !GEISHA_STUDIOS_LD23_SPRITES_CARD_HPP

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Critter.hpp"
#include <benzaiten/Game.hpp>

using namespace benzaiten;
using namespace ld23;

Critter::Critter (int dir_x, double speed)
    : Walker (dir_x, speed)
{
    this->type = Types::HAZARD;
    this->addWallType (Types::WALL_CRITTER);
    this->setHitbox (4, 8, 8, 16);
    this->setAnimation (Game::resources ().atlas ("critter.xml"));
}

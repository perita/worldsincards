//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_SPRITES_LABEL_HPP)
#define GEISHA_STUDIOS_LD23_SPRITES_LABEL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <benzaiten/Sound.hpp>
#include <benzaiten/Sprite.hpp>
#include <benzaiten/Timer.hpp>
#include <benzaiten/graphics/Text.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace ld23 {
    class Label: public benzaiten::Sprite {
        public:
            typedef boost::function<void (void)> Callback;
            typedef boost::shared_ptr<Label> Ptr;


            static Ptr New (double x, double y, const std::string &text,
                    Callback callback) {
                return Ptr (new Label (x, y, text, callback));
            }

            virtual void added ();

        protected:
            Label (double x, double y, const std::string &text,
                    Callback callback);

        private:
            bool showNextChar (benzaiten::Timer &timer);
            bool removeLabel (benzaiten::Timer &timer);

            Callback callback;
            benzaiten::Text::Ptr display;
            std::string::size_type length;
            benzaiten::Sound::Ptr sfx;
            std::string text;
    };
}

#endif // !GEISHA_STUDIOS_LD23_SPRITES_LABEL_HPP

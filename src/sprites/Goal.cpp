//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Goal.hpp"
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>

using namespace benzaiten;
using namespace ld23;

Goal::Goal (double x, double y)
    : Tenant (x, y)
    , observer (0)
    , sfx (Game::resources ().try_sound ("goal.ogg"))
{
    this->type = Types::GOAL;
    this->setLayer (2);
    this->setHitbox (-1, -1, 8, 8);
    this->setActive (false);
    this->setGraphic (Texture::New (Game::resources ().texture ("goal.tga")));
}

void Goal::moveToCard (Card &card) {
    this->moveTo (card.x () + this->x (), card.y () + this->y ());
}

void Goal::removed () {
    this->card->leave ();
    this->sfx->play ();
    if (this->observer) {
        this->observer->removed (*this);
    }
}

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Walker.hpp"
#include <boost/assign.hpp>

using namespace benzaiten;
using namespace ld23;

Walker::Walker (int dir_x, double walk_speed)
    : Tenant ()
    , animation ()
    , check_ladder (true)
    , current_speed (0.0)
    , dir (dir_x, 0)
    , on_ladder (false)
    , walk_speed (walk_speed)
    , wall_types (1, Types::WALL)
{
    this->setLayer (2);
}

void Walker::addWallType (int type) {
    this->wall_types.push_back (type);
}

void Walker::advanceCollideX (Sprite &sprite) {
    this->flip_x ();
}

void Walker::advanceCollideY (Sprite &sprite) {
    if (this->on_ladder) {
        this->continueWalking ();
    } else {
        this->current_speed = this->walk_speed;
    }
}

bool Walker::canClimbLadder (Sprite &ladder) const {
    return (ladder.getType () == Types::LEDDER_UP && this->cardUpLadder ()) ||
        (ladder.getType () == Types::LEDDER_DOWN && this->cardDownLadder ());
}

Card::Ptr Walker::cardDownLadder () const {
    Card::Ptr neighbor = this->card->getNeighbor (Card::BOTTOM);
    if (neighbor && neighbor->hasLadderUp ()) {
        return neighbor;
    }
    return Card::Ptr ();
}

Card::Ptr Walker::cardUpLadder () const {
    Card::Ptr neighbor = this->card->getNeighbor (Card::TOP);
    if (neighbor && neighbor->hasLadderDown ()) {
        return neighbor;
    }
    return Card::Ptr ();
}

void Walker::continueWalking () {
    this->on_ladder = false;
    this->check_ladder = false;
    this->animation->play (this->dir.x < 0 ? "left" : "right");
}

void Walker::exitBottom () {
    if (this->dir.y > 0) {
        if (!this->changeCard (this->cardDownLadder ())) {
            this->flip_y ();
        }
    }
}

void Walker::moveToCard (Card &card) {
    this->moveTo (card.x () + CARD_WIDTH / 2, card.y () + 8);
}

void Walker::exitLeft () {
    if (this->dir.x < 0) {
        this->check_ladder = true;
        if (!this->changeCard (this->card->getNeighbor (Card::LEFT))) {
            this->flip_x ();
        }
    }
}

void Walker::exitRight () {
    if (this->dir.x > 0) {
        this->check_ladder = true;
        if (!this->changeCard (this->card->getNeighbor (Card::RIGHT))) {
            this->flip_x ();
        }
    }
}

void Walker::exitTop () {
    if (this->dir.y < 0) {
        if (!this->changeCard (this->cardUpLadder ())) {
            this->flip_y ();
        }
    }
}

void Walker::flip_x () {
    if (this->dir.x < 0) {
        this->dir.x = 1;
        this->animation->play ("right");
    } else {
        this->dir.x = -1;
        this->animation->play ("left");
    }
}

void Walker::flip_y () {
    if (this->dir.y < 0) {
        this->dir.y = 1;
    } else if (this->dir.y > 0) {
        this->dir.y = -1;
    }
}

void Walker::setAnimation (const TextureAtlas::Ptr &atlas) {
    assert (atlas && "Invalid animation atlas");
    this->animation = atlas;
    animation->play (this->dir.x < 0 ? "left" : "right");
    animation->centerOrigin ();
    this->setGraphic (animation);
}

void Walker::update (double elapsed) {
    using boost::assign::list_of;

    static const std::vector<int> floor_types = list_of<int> (Types::FLOOR);
    static const std::vector<int> ladder_types =
            list_of<int> (Types::LEDDER_UP)(Types::LEDDER_DOWN);
    static const std::vector<int> ladder_end_up =
        list_of<int> (Types::LEDDER_UP_END);
    static const std::vector<int> ladder_end_down =
        list_of<int> (Types::LEDDER_DOWN_END);

    if (this->on_ladder) {
        this->advance (0.0, this->walk_speed * elapsed * this->dir.y,
                this->dir.y > 0 ? ladder_end_down : ladder_end_up);
    } else {
        this->current_speed = 0; // If on a floor, advanceCollideY will
                                 // re-set the speed.
        this->advance (0.0, GRAVITY * elapsed, floor_types);
        this->advance (this->current_speed * elapsed * this->dir.x,
                0.0, wall_types);
        if (this->check_ladder) {
            if (Sprite *ladder = this->collides (this->x (), this->y (),
                        ladder_types)) {
                if (this->canClimbLadder (*ladder)) {
                    this->moveToX (ladder->x ());
                    this->animation->play ("back");
                    this->on_ladder = true;
                    this->dir.y = ladder->getType () == Types::LEDDER_DOWN ? 1 : -1;
                } else {
                    this->check_ladder = false;
                }
            }
        }
    }
    this->card->checkExits (*this, this->dir.x, this->dir.y);
}

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "LevelOption.hpp"
#include <benzaiten/Game.hpp>
#include <boost/assign.hpp>

using namespace benzaiten;
using namespace ld23;

LevelOption::LevelOption (Observer &observer, int level, double x, double y)
    : Sprite (x, y)
    , atlas (TextureAtlas::New (
            Game::resources ().texture ("selection.tga"), 64, 64))
    , level (level)
    , observer (observer)
{
    using boost::assign::list_of;

    this->atlas->centerOrigin ();
    this->atlas->add ("uncompleted", 0);
    this->atlas->add ("completed", list_of<int> (0)(1)(3)(2)(4), 20, false);
    this->atlas->play ("uncompleted");
    this->setGraphic (this->atlas);

    this->setHitbox (32, 32, 64, 64);
}

void LevelOption::markAsCompleted () {
    this->atlas->play ("completed");
}

void LevelOption::update (double elapsed) {
    if (Game::input ().mouse_pressed &&
            this->collides (Point2D<double> (
                    Game::input ().mouse_x, Game::input ().mouse_y))) {
        this->observer.selected (*this);
    }
}

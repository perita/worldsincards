//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_SPRITES_GOAL_HPP)
#define GEISHA_STUDIOS_LD23_SPRITES_GOAL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <benzaiten/Sound.hpp>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>
#include "Tenant.hpp"

namespace ld23 {
    class Goal: public Tenant {
        public:
            typedef boost::shared_ptr<Goal> Ptr;

            struct Observer {
                virtual ~Observer () { }
                virtual void removed (Goal &) = 0;
            };

            static Ptr New (double x, double y) {
                return Ptr (new Goal (x, y));
            }

            void setObserver (Observer *observer) { this->observer = observer; }
            virtual void removed ();

        protected:
            Goal (double x, double y);
            virtual void moveToCard (Card &card);

        private:
            Observer *observer;
            benzaiten::Sound::Ptr sfx;
    };
}

#endif // !GEISHA_STUDIOS_LD23_SPRITES_GOAL_HPP

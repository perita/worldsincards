//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Label.hpp"
#include <algorithm>
#include <benzaiten/Context.hpp>
#include <benzaiten/Game.hpp>
#include <boost/bind.hpp>
#include "../Font.hpp"

using namespace benzaiten;
using namespace ld23;

namespace {
    const double TIME_CHAR = 0.01;
    const double MIN_TIME_SHOW = 2;
}

Label::Label (double x, double y, const std::string &text,
        Callback callback)
    : Sprite (x, y)
    , callback (callback)
    , display (Text::New (font::normal (), ""))
    , length (0)
    , sfx (Game::resources ().try_sound ("char.ogg"))
    , text (text)
{
    std::replace (this->text.begin (), this->text.end (), '|', '\n');
    this->moveToX (this->x () - font::normal ().width (this->text) / 2);
    this->setGraphic (this->display);
}

void Label::added () {
    this->context->addTimer (Timer::New (TIME_CHAR,
                boost::bind (&Label::showNextChar, this, _1)));
}

bool Label::showNextChar (Timer &timer) {
    if (this->length < this->text.size ()) {
        char next_char = this->text[this->length];
        ++this->length;
        this->display->setText (this->display->getText () + next_char);
        if (next_char == '\n') {
            return this->showNextChar (timer);
        }
        this->sfx->play ();
    } else {
        timer.setPeriod (
                std::max (MIN_TIME_SHOW, TIME_CHAR * this->length * 1.5));
        timer.setCallback (boost::bind (&Label::removeLabel, this, _1));
    }
    return true;
}

bool Label::removeLabel (Timer &timer) {
    if (this->callback) {
        this->callback ();
    }
    this->context->remove (this);
    return false;
}

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Player.hpp"
#include <cassert>
#include <benzaiten/Game.hpp>

using namespace benzaiten;
using namespace ld23;

Player::Player ()
    : Walker (-1, 40.0)
    , observer (0)
{
    this->type = Types::PLAYER;
    this->setHitbox (4, 8, 8, 16);
    this->setAnimation (Game::resources ().atlas ("player.xml"));
}

void Player::update (double elapsed) {
    if (this->collides (this->x (), this->y (), Types::HAZARD)) {
        if (this->observer) {
            this->observer->dead (*this);
        }
        this->setActive (false);
        return;
    } else if (Sprite *goal = this->collides (this->x (), this->y (),
                Types::GOAL)) {
        this->context->remove (goal);
    }

    Walker::update (elapsed);
}

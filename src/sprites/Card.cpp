//
// Ludum Dare 23.
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Card.hpp"
#include <cassert>
#include <benzaiten/Game.hpp>
#include <benzaiten/graphics/Texture.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include "Walker.hpp"

using namespace benzaiten;
using namespace ld23;

namespace piece {
    Sprite::Ptr new_piece (double x, double y, const std::string &name,
            int type, int hb_x, int hb_y, int hb_w, int hb_h) {
        TextureAtlas::Ptr atlas = Game::resources ().atlas ("pieces.xml");
        atlas->play (name);
        atlas->centerOrigin ();

        Sprite::Ptr piece = Sprite::New (x, y, atlas, type);
        piece->setActive (false);
        piece->setHitbox (hb_x, hb_y, hb_w, hb_h);

        return piece;
    }

    Sprite::Ptr new_checkpoint (double x, double y, int type, int hb_x,
            int hb_y, int hb_w, int hb_h) {
        Sprite::Ptr check = Sprite::New (x, y, Graphic::Ptr (), type);
        check->setActive (false);
        check->setHitbox (hb_x, hb_y, hb_w, hb_h);
        return check;
    }


    // -- Actual pieces --
    void empty (Card::Ptr &card) {
        card->add (
                new_checkpoint (0, 0, Types::WALL, 0, 0, CARD_WIDTH, CARD_HEIGHT));
    }

    void floor_0 (Card::Ptr &card) {
        card->add (new_piece (32, 25, "floor_0", Types::FLOOR, 31, 6, 62, 12));
    }

    void floor_1 (Card::Ptr &card) {
        card->add (new_piece (12, 25, "floor_1", Types::FLOOR, 11, 6, 23, 12));
    }

    void floor_2 (Card::Ptr &card) {
        card->add (new_piece (51, 25, "floor_2", Types::FLOOR, 11, 6, 23, 12));
    }

    void ladder_down (Card::Ptr &card) {
        card->add (new_piece (32, 24, "ladder_down", Types::LEDDER_DOWN,
                    2, 8, 4, 16));
        card->add (new_checkpoint (32, 25, Types::FLOOR, 6, 6, 12, 12));
        card->add (new_checkpoint (31, 2, Types::LEDDER_UP_END, 6, 1, 12, 2));
        card->setLadderDown (true);
    }

    void ladder_up (Card::Ptr &card) {
        card->add (new_piece (32, 10, "ladder_up", Types::LEDDER_UP, 2, 9, 4, 18));
        card->add (new_checkpoint (31, 16, Types::LEDDER_DOWN_END, 6, 1, 12, 2));
        card->setLadderUp (true);
    }
}

Card::Card ()
    : SpriteGroup ()
    , border ()
    , neighbor ()
    , tenants (0)
    , ladder_down (false)
    , ladder_up (false)
{
    this->setActive (false);
    this->setGraphic (Texture::New (Game::resources ().texture ("card.tga")));

    const int padding = 10;
    this->border[TOP] = this->add (piece::new_checkpoint (0, 0, -1,
                0, padding - 1, CARD_WIDTH, padding));
    this->border[RIGHT] = this->add (piece::new_checkpoint (CARD_WIDTH - 1, 0, -1,
                0, 0, padding, CARD_HEIGHT));
    this->border[BOTTOM] = this->add (piece::new_checkpoint (0, CARD_HEIGHT - 1, -1,
                0, 0, CARD_WIDTH, padding));
    this->border[LEFT] = this->add (piece::new_checkpoint (0, 0, -1,
                padding - 1, 0, padding, CARD_HEIGHT));
}

void Card::enter () {
    ++this->tenants;
}

void Card::checkExits (Walker &walker, int dir_x, int dir_y) {
    double x = walker.x ();
    double y = walker.y ();
    if (dir_x < 0 && walker.collides (x, y, *(this->border[LEFT]))) {
        walker.exitLeft ();
    } else if (dir_x > 0 && walker.collides (x, y, *(this->border[RIGHT]))) {
        walker.exitRight ();
    } else if (dir_y < 0 && walker.collides (x, y, *(this->border[TOP]))) {
        walker.exitTop ();
    } else if (dir_y > 0 && walker.collides (x, y, *(this->border[BOTTOM]))) {
        walker.exitBottom ();
    }
}

Card::Ptr Card::getNeighbor (Neighbor which) const {
    return this->neighbor[which];
}

void Card::leave () {
    assert (this->tenants > 0 && "Too many people left already!");
    --this->tenants;
}

Card::Ptr Card::New (int type) {
    Card::Ptr card (new Card ());

    switch (type) {
        // The empty card.
        case 0:
            piece::empty (card);
            break;

        // The simple card with a flat floor from right to left.
        case 1:
            piece::floor_0 (card);
            break;

        // A card with a ladder up.
        case 2:
            piece::floor_0 (card);
            piece::ladder_up (card);
            break;

        // A card with a ladder down.
        case 3:
            piece::floor_1 (card);
            piece::floor_2 (card);
            piece::ladder_down (card);
            break;

        // Floor at every side, and the middle is empty (player can fall)
        case 4:
            piece::floor_1 (card);
            piece::floor_2 (card);
            card->add (piece::new_checkpoint (24, 1, Types::WALL_CRITTER, 0, 0,
                        16, CARD_HEIGHT - 2));
            break;

        // Floor at every side and the middle is an inivisble wall (can't fall)
        case 5:
            piece::floor_1 (card);
            piece::floor_2 (card);
            card->add (piece::new_checkpoint (24, 1, Types::WALL, 0, 0,
                        16, CARD_HEIGHT - 2));
            break;

        default:
            assert (type != type && "Invalid card type");
            break;
    }
    return card;
}

void Card::setNeighbor (Neighbor which, const Card::Ptr &card) {
    this->neighbor[which] = card;
}

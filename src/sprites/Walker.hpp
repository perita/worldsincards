//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_LD23_SPRITES_HPP)
#define GEISHA_STUDIOS_LD23_SPRITES_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <benzaiten/Point2D.hpp>
#include <benzaiten/graphics/TextureAtlas.hpp>
#include "Tenant.hpp"

namespace ld23 {
    class Walker: public Tenant {
        public:
            virtual void advanceCollideX (Sprite &sprite);
            virtual void advanceCollideY (Sprite &sprite);

            void exitBottom ();
            void exitLeft ();
            void exitRight ();
            void exitTop ();
            virtual void update (double elapsed);

        protected:
            Walker (int dir_x, double walk_speed);

            void addWallType (int type);
            virtual void moveToCard (Card &card);
            void flip_x ();
            void flip_y ();
            bool canClimbLadder (Sprite &ladder) const;
            Card::Ptr cardDownLadder () const;
            Card::Ptr cardUpLadder () const;
            void continueWalking ();
            void setAnimation (const benzaiten::TextureAtlas::Ptr &atlas);

            benzaiten::TextureAtlas::Ptr animation;
            bool check_ladder;
            double current_speed;
            benzaiten::Point2D<int> dir;
            bool on_ladder;
            double walk_speed;
            std::vector<int> wall_types;
    };
}

#endif // !GEISHA_STUDIOS_LD23_SPRITES_HPP

//
// Ludum Dare 23
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "File.hpp"
#include <stdexcept>
#include "String.hpp"

using namespace ld23;

File::File (const std::string &file_name)
    : buffer (256, '\0')
    , buffer_size (0)
    , buffer_read (0)
    , input (SDL_RWFromFile (file_name.c_str (), "r"))
{
    if (this->input == 0) {
        throw std::invalid_argument (
                "Couldn't open file `" + file_name + "'");
    }
}

File::~File () {
    SDL_RWclose (this->input);
}

bool File::readLine (std::string &line) {
    line.clear ();
    for (;;) {
        if (this->buffer_read >= this->buffer_size) {
            this->buffer_size = SDL_RWread (this->input, &(this->buffer[0]),
                    1, this->buffer.size ());
            if (this->buffer_size < 0) {
                throw std::runtime_error ("Couldn't read data from file");
            }
            this->buffer_read = 0;
        }
        if (this->buffer_size == 0) {
            break;
        }
        char char_at_pos = '\0';
        while (this->buffer_read < this->buffer_size) {
            char_at_pos = this->buffer[this->buffer_read];
            if (char_at_pos == EOF) {
                break; // end of file, nothing else to do.
            } else if (char_at_pos == '\r') {
                ++this->buffer_read;
                continue; // Ignore Windows end-of-line characters.
            } else if (char_at_pos == '\n') {
                // Must increase to skip the character next time.
                ++this->buffer_read;
                break;
            } else {
                line.push_back (char_at_pos);
                ++this->buffer_read;
            }
        }

        if (char_at_pos == EOF || char_at_pos == '\n') {
            break;
        }
    }

    trim (line);

    return !line.empty ();
}

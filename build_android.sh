#!/bin/bash
set -e
for target in armeabi-v7a; do
    rm -fr build/${target}
    mkdir -p build/${target}
    cd build/${target}
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDTOOLCHAIN -DCMAKE_BUILD_TYPE="Release" -DARM_TARGET="${target}" -DFORCE_ARM=ON ../..
    make
    cd ../..
done
android update project --name "LD23" --target android-8 --path .
cd src
rm -f org
ln -fs ../lib/benzaiten/android/org org
cd ..
ant release

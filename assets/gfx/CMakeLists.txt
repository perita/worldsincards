if (NOT EMSCRIPTEN)

find_program(PIKSELO NAMES pikselo)

macro (EXPORT_IMAGE file_name list_to_append)
    set(_src_file "${CMAKE_CURRENT_SOURCE_DIR}/${file_name}.ora")
    set(_tga_file "${CMAKE_CURRENT_SOURCE_DIR}/${file_name}.tga")

    add_custom_command(
        OUTPUT ${_tga_file}
        COMMAND ${PIKSELO} --export ${_tga_file}
        ${_src_file}
        MAIN_DEPENDENCY ${_src_file}
        COMMENT "Exporting TGA image ${file_name}.tga")
    list(APPEND ${list_to_append} ${_tga_file})
endmacro (EXPORT_IMAGE)

macro (EXPORT_ANIMATION file_name frames list_tga list_xml)
    set(_tga_file "${CMAKE_CURRENT_SOURCE_DIR}/${file_name}.tga")
    set(_xml_file "${CMAKE_CURRENT_SOURCE_DIR}/${file_name}.xml")

    set(_frames "")
    foreach(frame ${frames})
        list(APPEND _frames "${CMAKE_CURRENT_SOURCE_DIR}/${frame}")
    endforeach(frame)

    add_custom_command(
        OUTPUT ${_tga_file} ${_xml_file}
        COMMAND ${PIKSELO} --export ${_tga_file} --map-file ${_xml_file} ${_frames}
        DEPENDS ${_frames}
        COMMENT "Exporting animation ${file_name}.tga")
    list(APPEND ${list_tga} ${_tga_file})
    list(APPEND ${list_xml} ${_xml_file})
endmacro (EXPORT_ANIMATION)

set(TGA_FILES )
set(XML_FILES )
set(PIECES
    pieces/floor_0.ora
    pieces/floor_1.ora
    pieces/floor_2.ora
    pieces/ladder_down.ora
    pieces/ladder_up.ora
    )
EXPORT_ANIMATION ("pieces" "${PIECES}" TGA_FILES XML_FILES)
EXPORT_ANIMATION ("player" "player/back.oaa;player/left.oaa;player/right.oaa"
    TGA_FILES XML_FILES)
EXPORT_ANIMATION ("critter" "critter/back.oaa;critter/left.oaa;critter/right.oaa"
    TGA_FILES XML_FILES)
EXPORT_ANIMATION ("selection" "selection.oaa" TGA_FILES XML_FILES)
EXPORT_ANIMATION ("lava" "lava.oaa" TGA_FILES XML_FILES)

EXPORT_IMAGE ("card" TGA_FILES)
EXPORT_IMAGE ("font-bold" TGA_FILES)
EXPORT_IMAGE ("font-normal" TGA_FILES)
EXPORT_IMAGE ("fwd" TGA_FILES)
EXPORT_IMAGE ("goal" TGA_FILES)
EXPORT_IMAGE ("oops" TGA_FILES)
EXPORT_IMAGE ("selectlevel" TGA_FILES)
EXPORT_IMAGE ("success" TGA_FILES)

add_custom_target (gfx ALL DEPENDS ${TGA_FILES} ${XML_FILES})

set (GFX_FILES ${TGA_FILES} ${XML_FILES}
    CACHE INTERNAL "List of graphic files fo install")
install (FILES ${GFX_FILES} DESTINATION ${RESOURCES_DIR}/gfx)

endif ()

set(STAGES
    01.txt
    02.txt
    03.txt
    04.txt
    05.txt
    )

string(REPLACE ";" "\n" STAGES_LIST "${STAGES}")
set(ALL_STAGES ${CMAKE_CURRENT_SOURCE_DIR}/all.txt)
file(WRITE ${ALL_STAGES} ${STAGES_LIST})

foreach(stage_file ${STAGES})
    list(APPEND ALL_STAGES ${CMAKE_CURRENT_SOURCE_DIR}/${stage_file})
endforeach(stage_file)
set(STAGE_FILES ${ALL_STAGES} CACHE INTERNAL "")

install(FILES ${ALL_STAGES} DESTINATION ${RESOURCES_DIR}/stages)

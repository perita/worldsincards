//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if !defined (GEISHA_STUDIOS_BALL_HPP)
#define GEISHA_STUDIOS_HEADER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL_stdinc.h>
#include <SDL_pixels.h>
#include <benzaiten/Sprite.hpp>
#include <boost/shared_ptr.hpp>

///
/// @class Ball
/// @brief Sprites that bounces on the screen's border.
///
/// The Ball sprite simply moves ahead until it reaches one of the four
/// screen's border, at which point it reverses its direction to move
/// to towards opposite border.
///
class Ball: public benzaiten::Sprite {
    public:
        /// A smart pointer to a Ball sprite.
        typedef boost::shared_ptr<Ball> Ptr;

        ///
        /// @brief Creates a smart pointer to a Ball sprite.
        ///
        /// @param[in] color The color to render the Ball with.
        ///
        /// @return The smart pointer to the newly created Ball.
        ///
        static Ptr New (const SDL_Color &color) {
            return Ptr (new Ball (color));
        }

        ///
        /// @brief Gets the Ball's color.
        ///
        /// @return The color used to render the ball.
        ///
        SDL_Color getColor () const { return this->color; }

        ///
        /// @brief Gets the Ball's radius.
        ///
        /// @return The radius, in pixels, of the Ball.
        ///
        double getRadius () const { return 4; }

        virtual void render (SDL_Renderer &renderer, const SDL_Rect *clip) const;
        virtual void update (double elapsed);

    protected:
        ///
        /// @brief Constructor
        ///
        /// @param[in] color The color to render the Ball with.
        ///
        Ball (const SDL_Color &color);

    private:
        /// The ball's color.
        SDL_Color color;
        /// The direction for the X (horizontal) coordinate.
        int dir_x;
        /// The direction for the Y (horizontal) coordinate.
        int dir_y;
};

#endif // !GEISHA_STUDIOS_BALL_HPP


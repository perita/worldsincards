//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Start.hpp"
#include <utility>
#include <benzaiten/BitmapFont.hpp>
#include <benzaiten/Game.hpp>
#include <benzaiten/ResourceManager.hpp>
#include <benzaiten/graphics/Text.hpp>
#include "Level.hpp"

Start::Start (unsigned int level, bool next):
    level (level)
{
    using benzaiten::BitmapFont;
    using benzaiten::Game;
    using benzaiten::Text;

    // The font to use to show the text.
    BitmapFont font (" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^abcdefghijklmnopqrstuvwxyz{}",
            Game::resources ().surface ("font.tga"));

    // Create an empty text and then set its value depending
    // on whether the player should repeat the same level, has
    // finished all the levels or should continue to a new level.
    Text::Ptr text = Text::New (font);
    if (next) {
        if (level < 11) {
            text->setText ("Tap to Begin New Level");
        } else {
            text->setText ("Well Done!");
        }
    } else {
        text->setText ("Oops! Try Again");
    }
    // Center the text--horizontally and vertically--to the center
    // of the screen (width / 2, height / 2).
    text->centerOrigin ();
    this->addGraphic (text,
            Game::screen ().half_width, Game::screen ().half_height);
}

benzaiten::Context::Ptr Start::levelToSwitch () {
    // Gets the balls (first) and the target (second) for the
    // next level based on the level number.  If we reached the end,
    // then return an empty Context to quit.
    std::pair<unsigned int, unsigned int> config;
    switch (this->level) {
        case 1:
            config = std::make_pair(4u, 1u);
            break;

        case 2:
            config = std::make_pair(4u, 2u);
            break;

        case 3:
            config = std::make_pair(8u, 4u);
            break;

        case 4:
            config = std::make_pair(15u, 7u);
            break;

        case 5:
            config = std::make_pair(25u, 12u);
            break;

        case 6:
            config = std::make_pair(30u, 17u);
            break;

        case 7:
            config = std::make_pair(30u, 20u);
            break;

        case 8:
            config = std::make_pair(30u, 25u);
            break;

        case 9:
            config = std::make_pair(30u, 28u);
            break;

        case 10:
            config = std::make_pair(30u, 30u);
            break;

        default:
            return Context::Ptr ();
            break;
    }

    return Level::New (this->level, config.first, config.second);
}

void Start::update (double elapsed) {
    using benzaiten::Action;
    using benzaiten::Game;

    // Always call the parent's update() function to update all its
    // sprites and internal structures.
    Context::update (elapsed);

    // If the Escape key (or similar key) is pressed, quit the game.
    if (Game::input ().pressed (Action::MENU_CANCEL)) {
        Game::quit ();
    }

    // If the mouse button is pressed, switch to the next level.
    if (Game::input ().mouse_pressed) {
        Game::switchTo (this->levelToSwitch ());
    }
}

//
// Brief Description
// Copyright (C) 2012 Geisha Studios
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "Level.hpp"
#include <iostream>
#include <benzaiten/ResourceManager.hpp>
#include <benzaiten/SpriteTypeIterator.hpp>
#include <benzaiten/Utils.hpp>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include "Start.hpp"
#include "../sprites/Ball.hpp"
#include "../sprites/Explosion.hpp"

namespace {
    /**
     * @brief Gets a random color.
     *
     * Each time this function is called, it returns a new color picked
     * randomly from a list of colors.
     *
     * As I have to give random colors to both Ball and Explosion sprites,
     * I prefer to have this function here and pass the color for Ball
     * or Explosion in the constructor.  Another way would be to move
     * this function to a separate file.
     *
     * @return A color chose at random.
     */
    SDL_Color rand_color () {
        using benzaiten::makeColor;
        using boost::assign::list_of;

        // This vector is only created once the first time this function
        // is called.
        static std::vector<SDL_Color> colors =
            list_of (makeColor (255,   0,   0)) // red
                    (makeColor (  0, 255,   0)) // lime
                    (makeColor (  0,   0, 255)) // blue
                    (makeColor (  0, 255, 255)) // aqua
                    (makeColor (255, 255,   0)) // yellow
                    (makeColor (255,   0, 255)); // fuchsia

        return colors[std::rand () % colors.size ()];
    }
}

Level::Level (unsigned int level, unsigned int balls, unsigned int target):
    first_explosion (false),
    level (level),
    removed (0),
    score (),
    target (target)
{
    // The font to use to draw the score.
    benzaiten::BitmapFont font (" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^abcdefghijklmnopqrstuvwxyz{}",
            benzaiten::Game::resources ().surface ("font.tga"));
    // Create the text without any initial content (i.e., empty string)
    // because I want to use the updateScore() function and avoid
    // duplication of code here.
    this->score = benzaiten::Text::New (font);
    this->updateScore ();
    // The text score is at position (10, 10) on the screen (top-left
    // corner.)
    this->addGraphic (this->score, 10, 10);

    // Add as many Ball sprites as told to.
    for (unsigned int ball = 0 ; ball < balls ; ++ball) {
        this->add (Ball::New (rand_color ()));
    }

}

void Level::update (double elapsed) {
    using benzaiten::Action;
    using benzaiten::Context;
    using benzaiten::Game;
    using benzaiten::Sprite;
    using benzaiten::spritesOfType;

    // Always call the parent's update() function to update all its
    // sprites and internal structures.
    Context::update (elapsed);

    // If the Escape key (or similar key) is pressed, quit the game.
    if (Game::input ().pressed (Action::MENU_CANCEL)) {
        Game::quit ();
    }

    // If the player already placed the first explosion, I won't allow
    // her any more.
    if (first_explosion) {
        bool update_score = false;

        // If there are no sprites with the Explosion type (defined in
        // config.h), then it means that the level is over.  Check whether
        // the player reached the target and pass the next level number to
        // the Start context if so.  Otherwise, pass the same level number
        // to Start and tell it that can't move to the next yet.
        Explosion *explosions = dynamic_cast<Explosion *>(
                this->firstOfType (Type::EXPLOSION));
        if (explosions == 0) {
            bool next = this->removed >= this->target;
            Game::switchTo (Start::New (this->level + (next ? 1 : 0), next));
        } else {
            // If there still are Explosion sprites, then I must check
            // whether a ball and an explosion are in collision (i.e., their
            // squared distance is less than the squared sum of their radii)
            // and remove the balls that collided.  The new explosion's
            // color and initial radius is the same as the ball because
            // it look better this way; otherwise I would see the ball
            // "disappear" and a 1-pixel explosion growing.
            Ball *balls = dynamic_cast<Ball *>(
                    this->firstOfType (Type::BALL));

            // spritesOfType returns a std::par<Sprite *, Sprite *>
            // with the Sprite type inferred from the parameter (Ball in
            // this case) and can be used in BOOST_FOREACH to loop over
            // all the sprites of the same type.
            //
            // However, keep in mind that for performance reasons, the
            // iterator does not use dynamic_cast<> but reinterpret_cast<>,
            // which means that if all the sprites are not of the same
            // class (derived are probably not acceptable) the behaviour is
            // not defined.
            //
            // In this case I know that all the sprites of Type::BALL are
            // of class Ball.  If that were not the case, then I would
            // made the `balls' variable of type `Sprite *' instead of
            // `Ball *'.  The same goes for the `explosions' variable.
            BOOST_FOREACH (Ball &ball, spritesOfType (balls)) {
                BOOST_FOREACH (Explosion &explosion, spritesOfType (explosions)) {
                    // This is the maximum distance to consider collision
                    // between an Explosion and a Ball.
                    double minDistanceSqrt =
                        (ball.getRadius() + explosion.getRadius()) *
                        (ball.getRadius() + explosion.getRadius());

                    // Using distanceSquared because I don't need the
                    // actual distance--only check if it is "small enough"--
                    // and sqrt() is expensive is not required.
                    if (ball.distanceSquaredTo(explosion) < minDistanceSqrt) {
                        // Add the new Explosion where the ball was.
                        this->add (Explosion::New (ball.x (), ball.y (),
                                        ball.getColor (), ball.getRadius()));
                        // And remove the ball.
                        this->remove (&ball);
                        this->removed++;
                        // The score text now must be updated.
                        update_score = true;
                        // And we don't need to check any other Explosion.
                        // Without the break here, as the Ball sprite is
                        // still alive, we could detect more collisions with
                        // nearby explosions creating more explosions than
                        // necessary (albeit all of them of the same color,
                        // size, and position, thus wouldn't be visible.)
                        break;
                    }
                }
            }
            // Only update the score if it is necessary, because it
            // needs to modify the texture each time which I try to
            // avoid whenever possible.
            if (update_score) {
                this->updateScore ();
            }
        }
    // If the player still didn't place the first explosion, but
    // the mouse button is pressed (or she tapped) in this frame,
    // place the first explosion and watch it chain-explode all
    // the balls that collide with it.
    } else if (Game::input().mouse_pressed) {
        this->add (Explosion::New (Game::input().mouse_x, Game::input().mouse_y,
                        rand_color ()));
        first_explosion = true;
    }
}

void Level::updateScore () {
    // Set the new text to be "remove/target"
    this->score->setText (boost::str(boost::format("%1%/%2%")
                % this->removed
                % this->target));
}

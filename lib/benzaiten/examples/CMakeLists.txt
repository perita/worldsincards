option(BENZAITEN_EXAMPLES "Whether to build the examples" OFF)
if (BENZAITEN_EXAMPLES)
    add_subdirectory(balls)
endif (BENZAITEN_EXAMPLES)

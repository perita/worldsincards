# Set the initial scaling factor based on the platform.
if (GP2X OR A320)
    set(_initial_factor 1)
else (GP2X OR A320)
    set(_initial_factor 2)
endif (GP2X OR A320)
set(SCREEN_SCALE ${_initial_factor} CACHE STRING "The screen scale factor")

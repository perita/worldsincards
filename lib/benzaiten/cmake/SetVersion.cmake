##
# Sets the version's variables.
#
# This will set the following CMake variables:
#
#   - VERSION_MAJOR: The major parameter.
#   - VERSION_MINOR: The minor parameter.
#   - VERSION_PATCH: The patch parameter.
#   - VERSION_REV: The working directory's changeset id.
#   - VERSION: Either major.minor.patch or major.minor.patch.rev,
#              depending on the platform.
#
macro(SET_VERSION _major _minor _patch)

    # Find the `hg' command line program and use it in order to get the local
    # working directory's revision.  The revision will be used in the last
    # component of the versions string.
    find_program(MERCURIAL_BIN NAMES hg.bat hg)
    if(MERCURIAL_BIN)
        execute_process(COMMAND ${MERCURIAL_BIN} tip
                        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                        OUTPUT_VARIABLE VERSION_REV)

        # Remove any extra version information, I only care about the revision
        # number.
        string(REGEX MATCH "^changeset:[ ]+[^;]+" VERSION_REV "${VERSION_REV}")
        string(REGEX MATCH "[0-9]+" VERSION_REV "${VERSION_REV}")
        if ("${VERSION_REV}" STREQUAL "")
            set(VERSION_REV "0")
        endif ("${VERSION_REV}" STREQUAL "")
    else(MERCURIAL_BIN)
        set(VERSION_REV "0")
    endif(MERCURIAL_BIN)

    # This is the application's version.
    set(VERSION_MAJOR ${_major})
    set(VERSION_MINOR ${_minor})
    set(VERSION_PATCH ${_patch})
    set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
    # On non-Apple builds, append the revision as the last component.
    # On Apple builds, we'll know it from the bundle version.
    if(NOT APPLE)
        set(VERSION "${VERSION}.${VERSION_REV}")
    endif(NOT APPLE)
endmacro(SET_VERSION)

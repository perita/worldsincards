# Boost's headers are required to build.
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

# Internal libraries' headers.
include_directories(${PROJECT_SOURCE_DIR}/lib/binreloc)
include_directories(${PROJECT_BINARY_DIR}/lib/SDL2)
include_directories(${PROJECT_SOURCE_DIR}/lib/SDL2/include)
include_directories(${PROJECT_SOURCE_DIR}/lib/SDL2_mixer)
include_directories(${PROJECT_SOURCE_DIR}/lib/tinyxml)

# Use STL for TinyXML.
add_definitions(-DTIXML_USE_STL)

# The library's sources for all platforms.
set(benzaiten_HEADERS
    Audio.hpp
    BitmapFont.hpp
    Context.hpp
    Graphic.hpp
    ErrorMessage.hpp
    FrameRateManager.hpp
    Game.hpp
    InputManager.hpp
    LoadTGA.hpp
    Music.hpp
    MusicMixer.hpp
    MusicNull.hpp
    Primitives.hpp
    ResourceManager.hpp
    Screen.hpp
    SDL.hpp
    SettingsBase.hpp
    Sound.hpp
    SoundMixer.hpp
    SoundNull.hpp
    Sprite.hpp
    SpriteGroup.hpp
    SpriteTypeIterator.hpp
    Timer.hpp
    Tweener.hpp
    Utils.hpp
    VirtualJoystick.hpp
    contexts/FadeTo.hpp
    contexts/Logo.hpp
    graphics/Animation.hpp
    graphics/StarField.hpp
    graphics/FillColor.hpp
    graphics/GraphicList.hpp
    graphics/Text.hpp
    graphics/Texture.hpp
    graphics/TextureAtlas.hpp
    graphics/TileMap.hpp
    tweeners/Group.hpp
    tweeners/Sequence.hpp
    tweeners/Simple.hpp
    tweeners/easing/Easing.hpp
    tweeners/easing/Back.hpp
    tweeners/easing/Bounce.hpp
    tweeners/easing/Elastic.hpp
    tweeners/easing/Linear.hpp
    tweeners/easing/None.hpp
    )

set(benzaiten_SOURCES
    Audio.cpp
    BitmapFont.cpp
    Context.cpp
    FrameRateManager.cpp
    Game.cpp
    InputManager.cpp
    MusicMixer.cpp
    LoadTGA.cpp
    Primitives.cpp
    ResourceManager.cpp
    Screen.cpp
    SettingsBase.cpp
    SoundMixer.cpp
    Sprite.cpp
    SpriteGroup.cpp
    Timer.cpp
    Tweener.cpp
    VirtualJoystick.cpp
    contexts/FadeTo.cpp
    contexts/Logo.cpp
    graphics/StarField.cpp
    graphics/FillColor.cpp
    graphics/GraphicList.cpp
    graphics/Text.cpp
    graphics/Texture.cpp
    graphics/TextureAtlas.cpp
    graphics/TileMap.cpp
    tweeners/Group.cpp
    tweeners/Sequence.cpp
    tweeners/Simple.cpp
    tweeners/easing/Back.cpp
    tweeners/easing/Bounce.cpp
    tweeners/easing/Elastic.cpp
    tweeners/easing/Linear.cpp
    )

# Linux sources.
if(UNIX AND NOT APPLE)
    list(APPEND benzaiten_HEADERS SettingsImplUnix.hpp)
    list(APPEND benzaiten_SOURCES SettingsImplUnix.cpp)
endif(UNIX AND NOT APPLE)

# Mac OS X sources.
if(APPLE)
    if (IOS)
        list (APPEND benzaiten_SOURCES ErrorMessage.m)
    else (IOS)
        list(APPEND benzaiten_HEADERS SettingsImplOSX.hpp)
        list(APPEND benzaiten_SOURCES SettingsImplOSX.cpp)
    endif (IOS)
endif(APPLE)

# Windows sources.
if(WIN32)
    list(APPEND benzaiten_HEADERS SettingsImplWin32.hpp)
    list(APPEND benzaiten_SOURCES SettingsImplWin32.cpp)
endif(WIN32)

# GP2X sources.
if(GP2X)
    list(APPEND benzaiten_HEADERS gp2x.hpp)
endif(GP2X)

# A320 sources.
if(A320)
    list(APPEND benzaiten_HEADERS a320.hpp)
endif(A320)

# Exceptions in Android.
if (ANDROID)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fexceptions")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fexceptions")
endif (ANDROID)

# Create the config.h file, add this directory to the include directories in
# order to find it, and tell the compiler to use it.
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/benzaiten_config.h.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/benzaiten_config.h @ONLY)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
add_definitions(-DHAVE_BENZAITEN_CONFIG_H)
list(APPEND benzaiten_HEADERS ${CMAKE_CURRENT_BINARY_DIR}/benzaiten_config.h)

# Tell that the header files are actually headers.
set_source_files_properties(${benzaiten_HEADERS} PROPERTIES HEADER_FILE_ONLY ON)

# Create an static library.
add_library(benzaiten STATIC ${benzaiten_HEADERS} ${benzaiten_SOURCES})

# Tell which libraries need to link against.
target_link_libraries(benzaiten SDL2_mixer SDL2 tinyxml)

# For Linux and such, use binary relocation.
if(UNIX AND NOT APPLE AND NOT GP2X AND NOT ANDROID AND NOT EMSCRIPTEN)
    target_link_libraries(benzaiten binreloc)
endif(UNIX AND NOT APPLE AND NOT GP2X AND NOT ANDROID AND NOT EMSCRIPTEN)

# XXX: For some odd reason, I have to link against the vorbis library
# under GP2X.
if(GP2X OR A320)
    target_link_libraries(benzaiten vorbisidec)
endif(GP2X OR A320)

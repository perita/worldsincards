//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SOUND_MIXER)
#define GEISHA_STUDIOS_BENZAITEN_SOUND_MIXER

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "SDL.hpp"
#include "Sound.hpp"

namespace benzaiten {
    ///
    /// @class SoundMixer
    /// @brief Uses SDL_mixer to play a sound.
    ///
    class SoundMixer: public Sound {
        public:
            ///
            /// @brief Creates a new intance of a Sound.
            ///
            /// @param[in] sound The smart pointer to the sound effect to
            ///            hold.  It must be a valid pointer.
            ///
            static Sound::Ptr New (SDL::Sound sound) {
                return Sound::Ptr (new SoundMixer (sound));
            }

            virtual void play() const;

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] sound The smart pointer to the sound effect to
            ///            hold.  It must be a valid pointer.
            ///
            SoundMixer (SDL::Sound sound);

        private:
            /// The real sound effect.
            SDL::Sound sound;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SOUND_MIXER

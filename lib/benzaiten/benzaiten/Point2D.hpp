//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_POINT2D_HPP)
#define GEISHA_STUDIOS_BENZAITEN_POINT2D_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

namespace benzaiten {
    ///
    /// @class Point2D
    /// @brief A 2D point in space.
    ///
    template <typename T> struct Point2D {
        /// The point's X position.
        T x;
        /// The point's Y position.
        T y;

        ///
        /// @brief Instantiates the Point2D.
        ///
        /// @param x The initial X position of the point.
        /// @param y The initial Y position of the point.
        ///
        explicit Point2D (T x = T(0), T y = T(0)):
            x (x),
            y (y)
        {
        }

        ///
        /// @brief Computes the squared distance to another Point2D.
        ///
        /// @param[in] other The other Point2D to compute the squared
        ///            distance to.
        ///
        /// @return The squared distance between this point and @p other.
        ///
        T distanceSquaredTo (const Point2D<T> &other) const {
            return (this->x - other.x) * (this->x - other.x) +
                   (this->y - other.y) * (this->y - other.y);
        }
    };

    ///
    /// @brief Helper function to create a new Point2D.
    ///
    /// This function exists so I don't have to specify the template
    /// parameter all the time.
    ///
    /// @param x The initial X position of the point.
    /// @param y The initial Y position of the point.
    ///
    /// @return A new Point2D.
    ///
    template<typename T> Point2D<T> makePoint2D (T x, T y) {
        return Point2D<T> (x, y);
    }
}

#endif // !GEISHA_STUDIOS_BENZAITEN_POINT2D_HPP

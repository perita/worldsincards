//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_VIRTUAL_JOYSTICK_HPP)
#define GEISHA_STUDIOS_BENZAITEN_VIRTUAL_JOYSTICK_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <SDL_rect.h>
#include <boost/shared_ptr.hpp>
#include "graphics/TextureAtlas.hpp"

namespace benzaiten {
    // Forward declarations.
    class InputManager;

    ///
    /// @class VirtualJoystick
    /// @brief An on-screen virtual joystick.
    ///
    /// The virtual joystick displays buttons on the screen--all of them from
    /// the same TextureAtlas--and can be set to the InputManager in order to
    /// convert clicks and touch events to joystick events when appropriate.
    ///
    class VirtualJoystick {
        public:
            /// The smart pointer to a virtual joystick.
            typedef boost::shared_ptr<VirtualJoystick> Ptr;

            ///
            /// @brief Creates a new VirtualJoystick.
            ///
            /// @param[in] atlas The texture atlas to use to draw the buttons.
            ///
            /// @return The smart pointer to the newly created virtual joystick.
            ///
            static Ptr New (const TextureAtlas::Ptr &atlas) {
                return Ptr (new VirtualJoystick (atlas));
            }

            ///
            /// @brief Adds a new button to the joystick.
            ///
            /// @param[in] action The action related to this button.
            /// @param[in] x The X coordinate to place the button.
            /// @param[in] y The Y coordinate to place the button.
            /// @param[in] frame The frame of the atlas to use to draw this button.
            ///
            void addButton (int action, int x, int y, unsigned int frame);

            ///
            /// @brief Cleans up the state of all the buttons.
            ///
            /// @param[in] input The input manager to use.
            ///
            void cleanUp (InputManager &input);

            ///
            /// @brief Handles a down event.
            ///
            /// @param[in] input The input manager to use.
            /// @param[in] pointer_id The id of the pointer that was pressed.
            ///
            /// @return @c true if the event was handled.
            ///
            bool handleDown (InputManager &input, int pointer_id);

            ///
            /// @brief Handles a up event.
            ///
            /// @param[in] input The input manager to use.
            /// @param[in] pointer_id The id of the pointer that was released.
            ///
            /// @return @c true if the event was handled.
            ///
            bool handleUp (InputManager &input, int pointer_id);

            ///
            /// @brief Renders the joystick onto the renderer.
            ///
            /// @param[inout] renderer The renderer to use to render the joystick.
            /// @param[in] clip The clip rectangle to use.
            ///
            void render (SDL_Renderer &renderer, const SDL_Rect *clip) const;

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] atlas The texture atlas to use to draw the buttons.
            ///
            VirtualJoystick (const TextureAtlas::Ptr &atlas);

        private:
            ///
            /// @struct Button
            /// @brief Information about a button.
            ///
            struct Button {
                /// The action this button triggers.
                int action;
                /// The frame within the atlas to use to draw this button.
                unsigned int frame;
                /// The id of the pointer device that presses the button.
                int pointer_id;
                /// Whether the button is pressed.
                bool pressed;
                /// The rectangle around the button, used to detect touches.
                SDL_Rect rect;

                ///
                /// @brief Constructor.
                ///
                /// @param action The action to assign to the button.
                /// @param freame The frame of this button.
                /// @param rect The button's position and size.
                ///
                Button (int action, unsigned int frame, const SDL_Rect &rect)
                    : action (action)
                    , frame (frame)
                    , pointer_id (0)
                    , pressed (false)
                    , rect (rect)
                {
                }
            };

            /// The type for the list of buttons.
            typedef std::vector<Button> ButtonList;

            /// The texture atlas to use to draw buttons.
            TextureAtlas::Ptr atlas;
            /// The defined buttons.
            ButtonList buttons;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_VIRTUAL_JOYSTICK_HPP

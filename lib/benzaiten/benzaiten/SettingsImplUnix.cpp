//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "SettingsImplUnix.hpp"
#include <fstream>
#include <libgen.h>
#include <string.h>
#include <unistd.h>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <sys/stat.h>
#include <sys/types.h>

using namespace benzaiten;

namespace
{
    ///
    /// @brief Recursively makes a directory path.
    ///
    /// @param[in] directory The directory path to make.
    ///
    void
    makeDirectory (const std::string &directory)
    {
        char *directoryPath = strdup (directory.c_str ());
        char *directoryToCheck = directoryPath;
        std::vector<std::string> directoriesToMake;
        struct stat statInfo;

        while ( -1 == stat (directoryToCheck, &statInfo) )
        {
            directoriesToMake.push_back (std::string (directoryToCheck));
            directoryToCheck = dirname (directoryToCheck);
        }
        free (directoryPath);

        for ( std::vector<std::string>::reverse_iterator dir = directoriesToMake.rbegin () ;
                dir != directoriesToMake.rend () ; ++dir )
        {
            mkdir (dir->c_str (), 0700);
        }
    }

    ///
    /// @brief Trims a string.
    ///
    /// @param[in] source The source string to trim.
    /// @param[in] delims The characters to strip out from @p source.
    /// @return A string without the leading and trailing characters specified
    ///         in @p delims.
    ///
    std::string
    trim (const std::string &source, const char *delims = " \t\r\n")
    {
        std::string result (source);
        std::string::size_type index = result.find_last_not_of (delims);
        if ( std::string::npos != index )
        {
            result.erase (++index);
        }
        index = result.find_first_not_of (delims);
        if ( std::string::npos != index )
        {
            result.erase (0, index);
        }
        else
        {
            result.erase ();
        }

        return result;
    }
}

SettingsImplUnix::SettingsImplUnix (const std::string &appName):
    SettingsBase::Impl (),
    fileName_ (),
    sections_ ()
{
    fileName_ = settingsFileName (appName);

    std::string fileName (fileName_);
    std::ifstream file (fileName.c_str ());
    std::string line;
    Section *section = 0;
    while ( std::getline (file, line) )
    {
        if ( line.length () > 0 and line[0] != '#' and line[1] != ';' )
        {
            if ( line[0] == '[' )
            {
                std::string sectionName (
                        trim (line.substr (1, line.find(']') - 1)));
                section = &(sections_[sectionName]);
            }
            else if ( 0 != section )
            {
                unsigned int equalPos = line.find('=');
                std::string name = trim (line.substr (0, equalPos));
                std::string value = trim (line.substr (equalPos + 1));

                (*section)[name] = value;
            }
        }
    }
}

SettingsImplUnix::~SettingsImplUnix ()
{
    std::ofstream file (fileName_.c_str ());
    for ( std::map<std::string, Section>::const_iterator section = sections_.begin () ;
            section != sections_.end () ; ++section )
    {
        file << "[" << section->first << "]\n";
        for (Section::const_iterator option = section->second.begin () ;
                option != section->second.end () ; ++option )
        {
            file << option->first << " = " << option->second << "\n";
        }
        file << "\n";
    }
    file.close ();
#if defined (GP2X) || defined (A320)
    // This is required in order to store the values to the SD card.
    sync ();
#endif // GP2X || A320
}

int
SettingsImplUnix::getInteger (const std::string &section,
        const std::string &name, int defaultValue) const
{
    try
    {
        const std::string notFoundString ("0xdeadbeef");
        std::string stringValue (getString (section, name, notFoundString));
        if ( stringValue != notFoundString )
        {
            return boost::lexical_cast<int> (stringValue);
        }
    }
    catch (boost::bad_lexical_cast &)
    {
        // failback to return the default value.
    }
    return defaultValue;
}

std::string
SettingsImplUnix::getString (const std::string &section,
        const std::string &name, const std::string &defaultValue) const
{
    std::map<std::string, Section>::const_iterator sectionIterator =
        sections_.find (section);
    if ( sections_.end () != sectionIterator )
    {
        Section::const_iterator option = sectionIterator->second.find (name);
        if ( sectionIterator->second.end () != option )
        {
            return option->second;
        }
    }
    return defaultValue;
}

void
SettingsImplUnix::setInteger (const std::string &section,
        const std::string &name, int value)
{
    try
    {
        setString (section, name, boost::lexical_cast<std::string> (value));
    }
    catch (boost::bad_lexical_cast &)
    {
        // Just swallow it.
    }
}

void
SettingsImplUnix::setString (const std::string &section,
        const std::string &name, const std::string &value)
{
    sections_[section][name] = value;
}

std::string
SettingsImplUnix::settingsDirectory (const std::string &appName) const
{
#if defined (GP2X)
    return std::string (".");
#else // !GP2X
    char *configHome = getenv ("XDG_CONFIG_HOME");
    std::string configDir (NULL == configHome ? "" : configHome);
    if ( 0 == configDir.length () )
    {
#if defined (A320)
        configDir += "/usr/local/home";
#else // !A320
        configDir += getenv ("HOME");
#endif // A320
        configDir += "/.config";
    }
    configDir += "/" + appName;
    makeDirectory (configDir);

    return configDir;
#endif
}

std::string
SettingsImplUnix::settingsFileName (const std::string &appName) const
{
    return settingsDirectory (appName) + "/settings.ini";
}

//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TIMER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TIMER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace benzaiten {
    ///
    /// @class Timer
    /// @brief Executes a callback after a certain time.
    ///
    /// This class is useful to call a certain function after a certain amount
    /// of time or to call it repeatedly at regular intervals.
    ///
    /// The callback function that is called must expect the pointer to the timer
    /// that called it (so it can change its properties) and also must tell
    /// whether the timer should be kept (i.e., return @c true) or not (@c false).
    ///
    class Timer {
        public:
            /// A smart pointer to a Timer.
            typedef boost::shared_ptr<Timer> Ptr;

            /// The callback function to call when the timer is up.
            typedef boost::function<bool (Timer &)> Callback;

            ///
            /// @brief Creates a new timer with a period and a callback function.
            ///
            /// @param[in] period The number of seconds to wait until the call
            ///            to the callback function.
            /// @param[in] callback The function to call.
            ///
            /// @return The smart pointer to the new Timer.
            ///
            static Ptr New (double period, Callback callback) {
                return Ptr (new Timer (period, callback));
            }

            ///
            /// @brief Tells whether the timer is finished.
            ///
            /// @return @c true if the Timer is finished, @c false otherwise.
            ///
            bool isFinished () const;

            ///
            /// @brief Resets the timer to the start of the period.
            ///
            void reset ();

            ///
            /// @brief Sets the Timer's callback function.
            ///
            /// @param[in] callback The callback function to set to the timer.
            ///
            void setCallback (Callback callback);

            ///
            /// @brief Sets the Timer's period.
            ///
            /// @param[in] period The new Timer's period in second.
            ///
            void setPeriod (double period);

            ///
            /// @brief Updates the timer.
            ///
            /// @param[in] elapsed The elapsed second since the last call to
            ///            this function.
            ///
            bool update (double elapsed);

        protected:
            ///
            /// @brief Constructor
            ///
            /// @param[in] period The number of seconds to wait until the call
            ///            to the callback function.
            /// @param[in] callback The function to call.
            ///
            Timer (double period, Callback callback);

        private:
            /// The callback function.
            Callback callback;
            /// The timer's current time.
            double current_time;
            /// The timer's period.
            double period;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_TIMER_HPP

//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "LoadTGA.hpp"
#include <string>
#include <stdexcept>
#include <SDL_rwops.h>
#include <boost/shared_ptr.hpp>

namespace benzaiten {
    struct TGAHeader {
        Uint8 id_length;
        Uint8 color_map_type;
        Uint8 image_type;
        Uint16 color_map_index;
        Uint16 color_map_length;
        Uint8 color_map_size;
        Uint16 origin_x;
        Uint16 origin_y;
        Uint16 width;
        Uint16 height;
        Uint8 pixel_size;
        Uint8 attributes;
    };

    ///
    /// @brief Helper function to close the RWops.
    ///
    /// I need this to feed boost::shared_ptr as SDL_RWclose is a macro.
    ///
    /// @param[in] rwops The SDL_RWops to close.
    ///
    void closeRW (SDL_RWops *rwops) {
        if (rwops != 0) {
            SDL_RWclose (rwops);
        }
    }

    SDL::Surface loadTGA (const std::string &file_name) {
        enum {
            TGA_TYPE_RGB = 2,
            TGA_TYPE_RLE_RGB = 10
        };

        boost::shared_ptr<SDL_RWops> file (
                SDL_RWFromFile (file_name.c_str (), "rb"),
                closeRW);
        if (!file) {
            std::string error ("Could not open TGA file: ");
            throw std::invalid_argument (error + file_name);
        }

        TGAHeader header;
        SDL_RWread (file.get (), &header.id_length, 1, 1);
        SDL_RWread (file.get (), &header.color_map_type, 1, 1);
        SDL_RWread (file.get (), &header.image_type, 1, 1);
        header.color_map_index = SDL_ReadLE16 (file.get ());
        header.color_map_length = SDL_ReadLE16 (file.get ());
        SDL_RWread (file.get (), &header.color_map_size, 1, 1);
        header.origin_x = SDL_ReadLE16 (file.get ());
        header.origin_y = SDL_ReadLE16 (file.get ());
        header.width = SDL_ReadLE16 (file.get ());
        header.height = SDL_ReadLE16 (file.get ());
        SDL_RWread (file.get (), &header.pixel_size, 1, 1);
        SDL_RWread (file.get (), &header.attributes, 1, 1);

        if (TGA_TYPE_RGB != header.image_type &&
                TGA_TYPE_RLE_RGB != header.image_type) {
            std::string error ("Could not load TGA file `");
            error += file_name + "': Not an RGB image.";
            throw std::invalid_argument (error);
        }

        if (0 != header.color_map_type ||
                (32 != header.pixel_size && 24 != header.pixel_size)) {
            std::string error ("Could not load TGA file `");
            error += file_name + "': Only 32 or 24 bit images are supported.";
            throw std::invalid_argument (error);
        }

        size_t bytes_per_pixel = (header.pixel_size + 7) >> 3;
        bool alpha = 4 == bytes_per_pixel;
        size_t width = header.width;
        size_t height = header.height;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
        Uint8 shift = alpha ? 0 : 8;
        Uint32 amask = 0x000000ff >> shift;
        Uint32 rmask = 0x0000ff00 >> shift;
        Uint32 gmask = 0x00ff0000 >> shift;
        Uint32 bmask = 0xff000000 >> shift;
#else // SDL_BYTEORDER != SDL_BIT_ENDIAN
        Uint32 amask = alpha ? 0xff000000 : 0;
        Uint32 rmask = 0x00ff0000;
        Uint32 gmask = 0x0000ff00;
        Uint32 bmask = 0x000000ff;
#endif // SDL_BYTEORDER == SDL_BIG_ENDIAN

        SDL::Surface image (SDL::surface (
                SDL_CreateRGBSurface (SDL_SWSURFACE, width, height,
                    bytes_per_pixel * 8, rmask, gmask, bmask, amask)));
        if (0 == image.get ()) {
            throw std::bad_alloc ();
        }

        SDL_RWseek (file.get (), header.id_length, RW_SEEK_CUR);

	if ( SDL_MUSTLOCK (image.get()) )
        {
            SDL_LockSurface (image.get());
        }
        ptrdiff_t stride = image->pitch;
        char *data = reinterpret_cast<char *> (image->pixels);
        // If the image is not stored from top to bottom, we need to change
        // the stride and data pointer.
        if (0x20 != (header.attributes & 0x20)) {
            data = data + (height - 1) * stride;
            stride = -stride;
        }

        bool rle = TGA_TYPE_RLE_RGB == header.image_type;
        size_t count = 0;
        size_t repetition = 0;
        for (size_t y = 0 ; y < height ; ++y) {
            if (rle) {
                Uint32 pixel;
                size_t x = 0;
                for (;;) {
                    if (count > 0) {
                        size_t actual_count = std::min (count, width - x);
                        count -= actual_count;
                        SDL_RWread (file.get (), data + x * bytes_per_pixel,
                                1, actual_count * bytes_per_pixel);
                        x += actual_count;
                        if (x == width) {
                            break;
                        }
                    } else if (repetition > 0) {
                        size_t actual_repetition =
                            std::min (repetition, width - x);
                        repetition -= actual_repetition;
                        while (actual_repetition > 0) {
                            SDL_memcpy (data + x * bytes_per_pixel, &pixel,
                                    bytes_per_pixel);
                            ++x;
                            --actual_repetition;
                        }
                        if (x == width) {
                            break;
                        }
                    }

                    Uint8 rle_data;
                    SDL_RWread (file.get (), &rle_data, 1, 1);
                    if (0x80 == (rle_data & 0x80)) {
                        repetition = (rle_data & 0x7f) + 1;
                        SDL_RWread (file.get (), &pixel, 1, bytes_per_pixel);
                    } else {
                        count = rle_data + 1;
                    }
                }
            } else {
                SDL_RWread (file.get (), data, 1, width * bytes_per_pixel);
            }
            data += stride;
        }
	if ( SDL_MUSTLOCK (image.get()) )
        {
            SDL_UnlockSurface (image.get());
        }

        return image;
    }
}

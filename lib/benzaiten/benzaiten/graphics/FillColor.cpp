//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "FillColor.hpp"
#include <SDL_render.h>

using namespace benzaiten;

FillColor::FillColor (const SDL_Color &color)
    : Graphic ()
    , alpha (SDL_ALPHA_OPAQUE)
    , color (color)
{
}

void FillColor::render (SDL_Renderer &renderer, const SDL_Rect *clip,
        int x, int y) const {
    SDL_SetRenderDrawColor (&renderer,
            this->color.r, this->color.g, this->color.b, this->alpha);
    SDL_BlendMode mode;
    SDL_GetRenderDrawBlendMode (&renderer, &mode);
    SDL_SetRenderDrawBlendMode (&renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderFillRect (&renderer, clip);
    SDL_SetRenderDrawBlendMode (&renderer, mode);
}

void FillColor::setAlpha (unsigned char alpha) {
    this->alpha = alpha;
}

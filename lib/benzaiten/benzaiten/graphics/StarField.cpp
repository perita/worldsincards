//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "StarField.hpp"
#include <algorithm>
#include <cassert>
#include <ctime>
#include <boost/foreach.hpp>
#include "../Utils.hpp"

using namespace benzaiten;

StarField::StarField (int width, int height)
    : Graphic ()
    , alpha (SDL_ALPHA_OPAQUE)
    , height (height)
    , layers ()
    // The value *must* be unsigned.
    , random_generator (static_cast<unsigned int>(std::time (0)))
    , width (width)
{
    assert (width > 0 && "The star field's width can't be 0");
    assert (height > 0 && "The star field's height can't be 0");
    this->setActive (true);
}

void StarField::add (unsigned int stars, const SDL_Color &color, int speed) {
    if (stars > 0) {
        RandomCoordinateGenerator randomX (this->random_generator,
                boost::uniform_int<> (0, this->width - 1));
        RandomCoordinateGenerator randomY (this->random_generator,
                boost::uniform_int<> (0, this->height - 1));
        this->layers.push_back (
                Layer (color, speed, stars, randomX, randomY));
    }
}

void StarField::render (SDL_Renderer &renderer, const SDL_Rect *clip,
        int x, int y) const {
    BOOST_FOREACH (const Layer &layer, this->layers) {
        SDL_SetRenderDrawColor (&renderer, layer.color.r, layer.color.g,
                layer.color.b, this->alpha);
        BOOST_FOREACH (const Point2D<double> &star, layer.stars) {
            if (clip == 0 || (star.x >= clip->x && star.x <= clip->x + clip->w &&
                    star.y >= clip->y && star.y <= clip->y + clip->h)) {
                SDL_RenderDrawPoint (&renderer, star.x + x, star.y + y);
            }
        }
    }
}

void StarField::update (double elapsed) {
    BOOST_FOREACH (Layer &layer, this->layers) {
        BOOST_FOREACH (Point2D<double> &star, layer.stars) {
            star.y += layer.speed * elapsed;
            while (star.y > this->height) {
                star.y -= this->height;
            }
        }
    }
}


////////////////////////////////
// Layer
////////////////////////////////
StarField::Layer::Layer(const SDL_Color &color, int speed,
        unsigned int num_stars, RandomCoordinateGenerator &randomX,
        RandomCoordinateGenerator &randomY):
    color(color),
    speed(speed),
    stars()
{
    for (size_t star = 0 ; star < num_stars ; ++star) {
        stars.push_back (Point2D<double> (randomX (), randomY ()));
    }
}

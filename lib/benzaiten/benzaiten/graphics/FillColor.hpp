//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GRAPHICS_FILL_COLOR_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GRAPHICS_FILL_COLOR_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "../Graphic.hpp"

namespace benzaiten {
    ///
    /// @class FillColor
    /// @brief Fills the screen with a color.
    ///
    class FillColor: public Graphic {
        public:
            /// A pointer to the FillColor.
            typedef boost::shared_ptr<FillColor> Ptr;

            ///
            /// @brief Creates a FillColor with the specified color.
            ///
            /// @param[in] color The color to use to fill the screen.
            ///
            static Ptr New (const SDL_Color &color) {
                return Ptr (new FillColor (color));
            }

            virtual void render (SDL_Renderer &render, const SDL_Rect *clip,
                    int x, int y) const;

            virtual void setAlpha (unsigned char alpha);

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param color The color to fill the screen with.
            ///
            FillColor (const SDL_Color &color);

        private:
            /// The alpha to use with the color.
            unsigned char alpha;

            /// The color to fill with.
            SDL_Color color;
    };
}

#endif // !GEISHA_STUDIOS_HEADER_HPP

//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "TextureAtlas.hpp"
#include <cassert>
#include "../Utils.hpp"

using namespace benzaiten;


TextureAtlas::TextureAtlas (const SDL::Texture &data, int width, int height)
    : Texture (data)
    , animation ()
    , animations ()
    , frame_index (-1)
    , frames ()
    , time (0.0)
{
    this->setActive (true);
    this->setUpFrames (width, height);
}

TextureAtlas::TextureAtlas (const SDL::Texture &data)
    : Texture (data)
    , animation ()
    , animations ()
    , frame_index (-1)
    , frames ()
    , time (0.0)
{
    this->setActive (true);
}

void TextureAtlas::add (const std::string &id, unsigned int frame) {
    std::vector<unsigned int> frames;
    frames.push_back (frame);
    this->add (id, frames, 0.0, false);
}

void TextureAtlas::add (const std::string &id,
        const std::vector<unsigned int> &frames, double rate, bool loop) {
    assert (!frames.empty ());
    this->animations[id] = Animation (id, frames, rate, loop);
}

int TextureAtlas::height () const {
    assert ( numFrames () > 0 && "There is no sprites in this sheet" );
    // Just return the height of the first sprite.  All sprites have
    // the same height.
    return this->frames[0].h;
}

unsigned int TextureAtlas::newFrame (int x, int y, int width, int height) {
    this->frames.push_back (makeRect (x, y, width, height));
    return this->frames.size () - 1;
}

unsigned int TextureAtlas::numFrames () const {
    return this->frames.size ();
}

void TextureAtlas::play (const std::string &id) {
    AnimationMap::iterator to_play = this->animations.find (id);
    if (to_play != this->animations.end()) {
        this->animation = to_play->second;
        this->time = 0.0;
        this->setFrame(this->animation.frame);
    } else {
        assert (false && "Animation not found");
    }
}

void TextureAtlas::setFrame (unsigned int index) {
    if (this->frame_index != index) {
        assert ( index < numFrames () && "Tried to set an invalid frame index");
        this->frame_index = index;
        SDL_Rect frame = this->frames[index];
        this->setClip (frame.x, frame.y, frame.w, frame.h);
    }
}

void TextureAtlas::setUpFrames (int width, int height) {
    if (height < 0) {
        height = width;
    }

    assert (width > 0 && "Invalid sprite sheet width");
    assert (height > 0 && "Index sprite sheet height");

    SDL_Rect texture_size = this->getFullSize ();
    const int cols = texture_size.w / width;
    const int rows = texture_size.h / height;
    for (int row = 0, y = 0; row < rows ; ++row, y += height) {
        for ( int col = 0, x = 0 ; col < cols ; ++col, x += width ) {
            this->newFrame (x, y, width, height);
        }
    }

    // By default, set the texture to the first frame.
    this->setClip (0, 0, width, height);
}

void TextureAtlas::update (double elapsed) {
    Texture::update (elapsed);
    this->time += this->animation.rate * elapsed;
    if (this->time > 1.0) {
        while (this->time > 1.0) {
            this->animation.next ();
            this->time -= 1.0;
        }
        this->setFrame(this->animation.frame);
    }
}

int TextureAtlas::width () const {
    assert ( numFrames () > 0 && "There is no sprites in this sheet" );
    // Just return the width of the first sprite.  All sprites have
    // the same height.
    return this->frames[0].w;
}

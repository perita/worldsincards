//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Text.hpp"
#include "Texture.hpp"

using namespace benzaiten;

Text::Text (const BitmapFont &font, const std::string &text)
    : Texture ()
    , alignment (LEFT)
    , font (font)
    , text ()
{
    if (!text.empty ()) {
        this->setText (text);
    }
}

void Text::alignRight () {
    if (this->alignment != RIGHT) {
        this->alignment = RIGHT;
        this->setText (this->text);
    }
}

void Text::centerOrigin () {
    Texture::centerOrigin ();
    this->alignment = CENTER;
}

std::string Text::getText () const {
    return text;
}

void Text::setText (const std::string &text) {
    this->text = text;
    this->setTexture (this->font.renderText (this->text));
    switch (this->alignment) {
        case CENTER:
            Texture::centerOrigin ();
            break;

        case RIGHT:
            this->origin.x = this->clip.w;
            this->origin.y = this->clip.h / 2;
            break;

        case LEFT:
        default:
            this->origin.x = 0;
            this->origin.y = this->clip.h / 2;
            break;
    }
}

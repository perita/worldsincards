//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP)
#define GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <vector>

namespace benzaiten {
    ///
    /// @class Animation.
    /// @brief An animation sequence.
    ///
    struct Animation {
            ///
            /// @brief Empty animation.
            ///
            Animation ()
                : frame (0)
                , frames (0)
                , id ()
                , index (0)
                , loop (false)
                , rate (0)
            {
            }

            ///
            /// @brief Initializes an animation.
            ///
            /// @param[in] id The identifier for the new animations.
            /// @param[in] frames The index of the frames that compose the
            ///            animation.
            /// @param[in] rate The animation's frame rate.
            /// @param[in] loop Whether the animation starts again or stays
            ///            at the last frame when done.
            ///
            Animation (const std::string &id,
                    const std::vector<unsigned int> &frames, double rate,
                    bool loop)
                : frame (frames[0])
                , frames (frames)
                , id (id)
                , index(0)
                , loop (loop)
                , rate (rate)
            {
            }

            ///
            /// @brief Updates the animation with the next frame.
            ///
            void next () {
                if (!this->frames.empty ()) {
                    if (this->index < this->frames.size () - 1) {
                        ++this->index;
                    } else if (this->loop) {
                        this->index = 0;
                    }
                    this->frame = this->frames[this->index];
                }
            }

            unsigned int frame;
            std::vector<unsigned int> frames;
            std::string id;
            size_t index;
            bool loop;
            double rate;
            bool active;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_ANIMATION_HPP

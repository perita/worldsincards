//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SETTINGS_IMPL_WIN32_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SETTINGS_IMPL_WIN32_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "SettingsBase.hpp"
#include <windows.h>

namespace benzaiten
{
    ///
    /// @class SettingsImplWin32
    /// @brief Low level implementation of SettingsBase for Win32.
    ///
    class SettingsImplWin32: public SettingsBase::Impl
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] appName The application's name.
            ///
            SettingsImplWin32 (const std::string &appName);

            virtual int getInteger (const std::string &section,
                const std::string &name, int defaultValue) const;
            virtual std::string getString(const std::string &section,
                const std::string &name,
                const std::string &defaultValue) const;
            virtual void setInteger (const std::string &section,
                const std::string &name, int value);
            virtual void setString(const std::string &section,
                const std::string &name, const std::string &value);

        private:
            /// The registry subkey to use to read / write.
            const std::string registrySubKey_;

            ///
            /// @brief Writes a value to the registry.
            ///
            /// @param[in] section The section to write the value to.
            /// @param[in] name The name to write the value to.
            /// @param[in] type The type of @p value.
            /// @param[in] value The pointer to the value to write.
            /// @param[in] size The size of @p value in bytes.
            ///
            void writeValue(const std::string &section, const std::string &name,
                DWORD type, BYTE *value, DWORD valueSize);
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SETTINGS_IMPL_WIN32_HPP

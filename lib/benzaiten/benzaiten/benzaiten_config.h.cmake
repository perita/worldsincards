/*
 * Benzaiten - A Simple Game Framework.
 * Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
 *
 * This library is free software;  you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#if !defined (GEISHA_STUDIOS_BENZAITEN_CONFIG_H)
#define GEISHA_STUDIOS_BENZAITEN_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* The path to the data directory. */
#define PACKAGE_DATA_DIR "@CMAKE_INSTALL_PREFIX@/share"

/* Define to 1 if compiling for Windows. */
#cmakedefine WIN32 1

/* Define to 1 if compiling for Mac OS X. */
#cmakedefine APPLE 1

/* Define to 1 if compiling for GP2X */
#cmakedefine GP2X 1

/* Define to 1 if compiling for A320 */
#cmakedefine A320 1

/* Define to 1 if compiling for Android */
#cmakedefine ANDROID 1

/* Define to 1 if compiling for iOS */
#cmakedefine IOS 1

/* Define to 1 if compiling for an UNIX variant (even Mac OS X.) */
#cmakedefine UNIX 1

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !GEISHA_STUDIOS_BENZAITEN_CONFIG_H */

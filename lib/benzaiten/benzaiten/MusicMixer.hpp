//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_MUSIC_MIXER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_MUSIC_MIXER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "SDL.hpp"
#include "Music.hpp"

namespace benzaiten {
    ///
    /// @class MusicMixer
    /// @brief A music that player with SDL_Mixer.
    ///
    class MusicMixer: public Music {
        public:
            ///
            /// @brief Creates a new MusicMixer instance.
            ///
            /// @param[in] music The smart pointer to the music to hold.
            ///            It must be a valid pointer.
            ///
            static Music::Ptr New (SDL::Music music) {
                return Music::Ptr (new MusicMixer (music));
            }

            virtual void halt();
            virtual void pause();
            virtual void play(int times = Infinite);
            virtual void resume();

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] music The smart pointer to the music to hold.
            ///            It must be a valid pointer.
            ///
            MusicMixer(SDL::Music music);

        private:
            /// The real music object.
            SDL::Music music;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_MUSIC_MIXER_HPP

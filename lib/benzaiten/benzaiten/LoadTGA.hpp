//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_LOAD_TGA_HPP)
#define GEISHA_STUDIOS_BENZAITEN_LOAD_TGA_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include "SDL.hpp"

namespace benzaiten {
    ///
    /// @brief Loads a TGA file into a surface.
    ///
    /// @param[in] file_name The name of the TGA file to load.
    ///
    /// @return The (smart) pointer to an SDL surface with the TGA file named
    ///         @p fileName loaded inside.
    ///
    /// @throw std::invalid_argument If @p file_name could not be loaded.
    ///
    SDL::Surface loadTGA (const std::string &file_name);
}

#endif // !GEISHA_STUDIOS_BENZAITEN_LOAD_TGA_HPP

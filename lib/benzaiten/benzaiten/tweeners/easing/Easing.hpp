//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TWEENERS_EASING_EASING_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TWEENERS_EASING_EASING_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

namespace benzaiten { namespace easing {
    ///
    /// @struct Easing
    /// @brief Easing functions for the Tweener.
    ///
    template <double (*Function)(double)> struct Easing {
        static double easeIn (double time) {
            return Function (time);
        };

        static double easeInOut (double time) {
            return time < 0.5 ?
                easeIn (2.0 * time) / 2.0 :
                easeOut (2 * time - 1) / 2.0 + 0.5;
        }

        static double easeOut (double time) {
            return 1.0 - easeIn (1.0 - time);
        }
    };
} }

#endif // !GEISHA_STUDIOS_BENZAITEN_TWEENERS_EASING_EASING_HPP


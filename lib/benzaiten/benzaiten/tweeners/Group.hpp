//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TWEENERS_GROUP_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TWEENERS_GROUP_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <boost/shared_ptr.hpp>
#include "../Tweener.hpp"

namespace benzaiten { namespace tweener {
    ///
    /// @class Group
    /// @brief Executes a group of Tweener in parallel
    ///
    class Group: public Tweener {
        public:
            /// A smart pointer to a Group tweener.
            typedef boost::shared_ptr<Group> Ptr;

            ///
            /// @brief Creates a new Group tweener.
            ///
            /// @return The smart pointer to the new group.
            ///
            static Ptr New () { return Ptr (new Group ()); }

            ///
            /// @brief Adds a Tweener to the group.
            ///
            /// @param[in] tweener The tweener to add.
            ///
            /// @return The added Tweener.
            ///
            Tweener::Ptr add (const Tweener::Ptr &tweener);

            virtual bool isFinished () const;

        protected:
            ///
            /// @brief Constructor.
            ///
            Group ();

            virtual void doUpdate (double elapsed);

        private:
            /// The list of tweeners to execute in parallel.
            std::list<Tweener::Ptr> tweeners;
    };
} }

#endif // !GEISHA_STUDIOS_BENZAITEN_TWEENERS_GROUP_HPP

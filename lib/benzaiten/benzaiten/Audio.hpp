//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_AUDIO_HPP)
#define GEISHA_STUDIOS_BENZAITEN_AUDIO_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL_mixer.h>
#include <boost/noncopyable.hpp>

namespace benzaiten {
    ///
    /// @class Audio
    /// @brief The Audio subsystem.
    ///
    class Audio: public boost::noncopyable {
        public:
            enum {
                DefaultFrequency = MIX_DEFAULT_FREQUENCY
            };

            ///
            /// @enum Channels
            /// @brief The number of audio channels to use.
            ///
            enum Channels {
                /// A single channel.
                Mono = 1,
                /// Two channels.
                Stereo = 2
            };

            ///
            /// @enum Format
            /// @brief The format to use for audio samples.
            ///
            enum Format {
                /// Unsigned 8-bit samples.
                U8 = AUDIO_U8,
                /// Signed 8-bit samples.
                S8 = AUDIO_S8,
                /// Unsigned 16-bit samples, in little-endian byte order.
                U16LSB = AUDIO_U16LSB,
                /// Signed 16-bit samples, in little-endian byte order.
                S16LSB = AUDIO_S16LSB,
                /// Unsigned 16-bit samples, in big-endian byte order.
                U16MSB = AUDIO_U16MSB,
                /// Signed 16-bit samples, in big-endian byte order.
                S16MSB = AUDIO_S16MSB,
                /// Unsigned 16-bit samples, in system byte order.
                U16SYS = AUDIO_U16SYS,
                /// Signed 16-bit samples, in system byte order.
                S16SYS = AUDIO_S16SYS
            };

            ///
            /// @brief Constructor.
            ///
            /// @param[in] frequency Output sampling frequency in samples per
            ///            second (Hz.)
            /// @param[in] format Output sample format.
            /// @param[in] channels Number of sound channels in output.  Either
            ///            mono or stereo.
            /// @param[in] chunksize Bytes used per output sample.
            ///
            Audio (int frequency, Format format, Channels channels,
                    size_t chunksize);

            ///
            /// @brief Destructor.
            ///
            ~Audio ();
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_AUDIO_HPP

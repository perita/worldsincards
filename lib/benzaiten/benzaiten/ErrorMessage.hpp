//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_ERROR_MESSAGE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_ERROR_MESSAGE_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL_config.h>

#if defined (__ANDROID__)
#include <android/log.h>
#elif defined (__WIN32__)
#include <windows.h>
#else
#include <iostream>
#endif

#if defined (__IPHONEOS__)
extern "C" void show_error_ios (const char *text);
#endif // __IPHONEOS__

namespace benzaiten
{
    ///
    /// @brief Shows an error text.
    ///
    /// @param[in] text The error text to to show.
    ///
   inline void showError (const std::string &text) {
#   if defined (__ANDROID__)
       __android_log_print (ANDROID_LOG_WARN, "Benzaiten", text.c_str ());
#elif defined (__IPHONEOS__)
       show_error_ios (text.c_str ());
# elif defined (__WIN32__)
       MessageBox (NULL, text.c_str (), "Error", MB_OK | MB_ICONERROR);
#else
       std::cerr << text << std::endl;
#endif
    }

    ///
    /// @brief Shows an error message in the most appropriate way.
    ///
    /// Show an error message using the native interface of the platform
    /// the game is runnin in.
    ///
    /// @param[in] message The error message to show.
    ///
    inline void showErrorMessage (const std::string &message) {
#if defined (WIN32)
        showError (message);
#else // !WIN32
        showError (std::string ("Error: ") + message);
#endif // WIN32
    }

    ///
    /// @brief Shows a message of unknown error.
    ///
    /// This is to be used when there has been an error but there was not a
    /// known cause.  It shows a generic error message using the most appropriate
    /// means accordint to the platform.
    ///
    inline void showUnknownErrorMessage () {
        showError ("Unknown error");
    }
}

#endif // !GEISHA_STUDIOS_BENZAITEN_ERROR_MESSAGE_HPP

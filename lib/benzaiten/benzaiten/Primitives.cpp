//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Primitives.hpp"
#include <SDL_render.h>

namespace benzaiten {

void renderFillCircle (SDL_Renderer &renderer, int cx, int cy,
        int radius, const SDL_Color &color, const SDL_Rect *clip) {
    if (radius < 1) {
        return;
    }

    int error = -radius;
    int x = radius;
    int y = 0;

    SDL_SetRenderDrawColor (&renderer, color.r, color.g, color.b,
            SDL_ALPHA_OPAQUE);
    while (x >= y) {
        SDL_Rect rect;

        rect.h = 1;
        rect.w = 2 * x;
        rect.x = cx - x;
        rect.y = cy - y;
        if (clip == 0 || SDL_IntersectRect (&rect, clip, &rect)) {
            SDL_RenderFillRect(&renderer, &rect);
        }

        rect.h = 1;
        rect.w = 2 * x;
        rect.x = cx - x;
        rect.y = cy + y;
        if (clip == 0 || SDL_IntersectRect (&rect, clip, &rect)) {
            SDL_RenderFillRect(&renderer, &rect);
        }

        rect.h = 1;
        rect.w = 2 * y;
        rect.x = cx - y;
        rect.y = cy - x;
        if (clip == 0 || SDL_IntersectRect (&rect, clip, &rect)) {
            SDL_RenderFillRect(&renderer, &rect);
        }

        rect.h = 1;
        rect.w = 2 * y;
        rect.x = cx - y;
        rect.y = cy + x;
        if (clip == 0 || SDL_IntersectRect (&rect, clip, &rect)) {
            SDL_RenderFillRect(&renderer, &rect);
        }

        error += y;
        ++y;
        error += y;
        if (error >= 0) {
            --x;
           error -= 2 * x;
        }
    }
}

} // namespace

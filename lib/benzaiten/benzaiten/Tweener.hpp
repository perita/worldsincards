//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_TWEENER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_TWEENER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace benzaiten {
    ///
    /// @class Tweener
    /// @brief Makes a value to change throught time.
    ///
    /// The Tweener changesd the value of a variable or a class' attribute
    /// from an initial value to a final value according to the expected
    /// duration of the progression and the result of a given function.
    ///
    /// When the value reached its end value, it is possible to set the up
    /// tweener to call a callback function.
    ///
    class Tweener {
        public:
            /// A smart pointer to a Tweener.
            typedef boost::shared_ptr<Tweener> Ptr;

            /// The callback function to call when the tweener ends.
            typedef boost::function<void ()> FinishCallback;

            ///
            /// @brief Destructor.
            ///
            virtual ~Tweener () { }

            ///
            /// @brief Tells whether the tweener is finished.
            ///
            /// @return @c true if the Tweener is finished.
            ///
            virtual bool isFinished () const = 0;

            ///
            /// @brief Calls the finished callback.
            ///
            void notifyFinish () {
                if (this->finish_callback) {
                    this->finish_callback ();
                }
            }

            ///
            /// @brief Sets the function to call when the Tweener is finished.
            ///
            /// @param[in] callback The function to call.
            ///
            void setFinishCallback (FinishCallback callback) {
                this->finish_callback = callback;
            }

            ///
            /// @brief Sets the Tweener's delay in seconds.
            ///
            /// @param[in] delay The tweener's delay in seconds.
            ///
            void setDelay (double delay) {
                this->delay = delay;
            }

            ///
            /// @brief Updates the value according to the elapsed time.
            ///
            /// @param[in] elapsed The elapsed seconds since the last call
            ///            to this function.
            ///
            void update (double elapsed);

        protected:
            ///
            /// @brief Constructor.
            ///
            Tweener ();

            ///
            /// @brief Updates the value according to the elapsed time.
            ///
            /// @param[in] elapsed The elapsed seconds since the last call
            ///            to this function.
            ///
            virtual void doUpdate (double elapsed) = 0;

        private:
            /// The callback to use when the tweener is finished.
            FinishCallback finish_callback;
            /// The delay before starting the tweener's effect.
            double delay;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_TWEENER_HPP

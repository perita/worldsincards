//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "BitmapFont.hpp"
#include <algorithm>
#include <cassert>
#include <iterator>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include "Game.hpp"

using namespace benzaiten;

namespace {

    ///
    /// @class ScopedLock
    /// @brief Locks the access to a surface's pixels.
    ///
    class ScopedLock: public boost::noncopyable {
        public:
            ScopedLock (SDL_Surface *surface):
                surface (surface)
            {
                if ( SDL_MUSTLOCK (this->surface) ) {
                    SDL_LockSurface (this->surface);
                }
            }

            ~ScopedLock () {
                if ( SDL_MUSTLOCK (this->surface) ) {
                    SDL_UnlockSurface (this->surface);
                }
            }

        private:
            SDL_Surface *surface;
    };


    Uint32 getPixel (SDL_Surface *surface, int x, int y) {
        assert (surface != 0);
        assert ( x > -1 && x < surface->w );
        assert ( y > -1 && y < surface->h );

        Uint32 pixelColor = 0;
        ScopedLock lock (surface);
        switch ( surface->format->BytesPerPixel )
        {
            case 1:
                pixelColor = static_cast<uint32_t>(
                        *(static_cast<uint8_t *> (surface->pixels) + y * surface->pitch + x));
                break;

            case 2:
                pixelColor = static_cast<uint32_t> (
                        *(static_cast<uint16_t *> (surface->pixels) + y * surface->pitch / 2 + x));
                break;

            case 3:
                {
                    uint8_t *pixels = static_cast<uint8_t *> (surface->pixels) +
                        y * surface->pitch + x * 3;
                    uint8_t red = *(pixels + surface->format->Rshift / 8);
                    uint8_t green = *(pixels + surface->format->Gshift / 8);
                    uint8_t blue = *(pixels + surface->format->Bshift / 8);
                    pixelColor = SDL_MapRGBA (surface->format, red, green, blue, 255);
                }
                break;

            case 4:
                pixelColor = *(static_cast<uint32_t *> (surface->pixels) +
                        y * surface->pitch / 4 + x);
                break;
        }

        return pixelColor;
    }


}

BitmapFont::BitmapFont (const std::string &characters, SDL::Surface surface):
    characters (),
    surface (surface)
{
    const uint32_t separator = getPixel (surface.get (), 0, 0);

    uint16_t x = 1;
    std::string::const_iterator character = characters.begin ();
    while ( x < surface->w && character != characters.end () )
    {
        if ( separator != getPixel (surface.get (), x, 0) )
        {
            SDL_Rect rect;
            rect.x = x;
            rect.y = 0;
            rect.h = surface->h;
            rect.w = 0;
            while ( (x < surface->w) &&
                    (separator != getPixel (surface.get (), x, 0)) )
            {
                ++x;
            }
            rect.w = x - rect.x;
            this->characters[*character] = rect;
            ++character;
        }
        ++x;
    }
}

SDL::Texture BitmapFont::renderText (const std::string &text) const {
    SDL_Rect dest = {0, 0, 0, height ()};
    int x = 0;
    for ( std::string::const_iterator character = text.begin () ;
            character != text.end () ; ++character ) {
        if ( L'\n' == *character ) {
            dest.w = std::max (dest.w, x);
            dest.h += height ();

            x = 0;
        } else {
            std::map<std::string::value_type, SDL_Rect>::const_iterator item =
                this->characters.find (*character);
            if ( item != this->characters.end () ) {
                const SDL_Rect &rect (item->second);
                x += rect.w;
            }
        }
    }
    dest.w = std::max (dest.w, x);

    SDL::Surface text_surface (SDL::surface (
            SDL_CreateRGBSurface (0, dest.w, dest.h,
                this->surface->format->BitsPerPixel,
                this->surface->format->Rmask,
                this->surface->format->Gmask,
                this->surface->format->Bmask,
                this->surface->format->Amask)));
    Uint32 transparent_color =
        SDL_MapRGBA (text_surface->format, 255, 0, 255, SDL_ALPHA_TRANSPARENT);
    SDL_FillRect (text_surface.get (), 0, transparent_color);

    SDL_BlendMode blend_mode;
    SDL_GetSurfaceBlendMode (this->surface.get (), &blend_mode);
    dest.x = 0;
    dest.y = 0;
    SDL_SetSurfaceBlendMode(this->surface.get (), SDL_BLENDMODE_NONE);
    for ( std::string::const_iterator character = text.begin () ;
            character != text.end () ; ++character ) {
        if ( L'\n' == *character ) {
            dest.x = 0;
            dest.y += height ();
        } else {
            std::map<std::string::value_type, SDL_Rect>::const_iterator item =
                this->characters.find (*character);
            if ( item != this->characters.end () )
            {
                const SDL_Rect &source (item->second);
                SDL_BlitSurface (this->surface.get (), &source,
                        text_surface.get (), &dest);
                dest.x += source.w;
            }
        }
    }
    SDL_SetSurfaceBlendMode(this->surface.get (), blend_mode);

    SDL_SetColorKey (text_surface.get (), SDL_TRUE, transparent_color);
    return SDL::texture (Game::screen ().renderer.get (), text_surface);
}

int BitmapFont::height () const {
    return this->surface->h;
}

int BitmapFont::width (const std::string &text) const {
    uint16_t totalWidth = 0;
    uint16_t width = 0;
    for ( std::string::const_iterator character = text.begin () ;
            character != text.end () ; ++character ) {
        if ( L'\n' != *character ) {
            std::map<std::string::value_type, SDL_Rect>::const_iterator item =
                this->characters.find (*character);
            if ( item != this->characters.end () )
            {
                width += item->second.w;
            }
        }
        else
        {
            totalWidth = std::max (width, totalWidth);
            width = 0;
        }
    }

    return std::max (width, totalWidth);
}

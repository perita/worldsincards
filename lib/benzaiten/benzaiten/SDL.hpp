//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SDL_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SDL_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <SDL_mixer.h>
#include <SDL_render.h>
#include <SDL_video.h>
#include <boost/shared_ptr.hpp>

// Forward declarations.
struct SDL_Surface;

namespace benzaiten {
    namespace SDL {
        /// The pointer to a music resource.
        typedef boost::shared_ptr<Mix_Music> Music;
        /// The pointer to a renderer.
        typedef boost::shared_ptr<SDL_Renderer> Renderer;
        /// The pointer to a sound resource.
        typedef boost::shared_ptr<Mix_Chunk> Sound;
        /// The pointer to a surface.
        typedef boost::shared_ptr<SDL_Surface> Surface;
        /// The pointer to a texture.
        typedef boost::shared_ptr<SDL_Texture> Texture;
        /// The pointer to a Window.
        typedef boost::shared_ptr<SDL_Window> Window;

        ///
        /// @brief Creates a new Music from a file.
        ///
        /// @param[in] file_name The name of the music file to load.
        ///
        /// @return The smart pointer to the loaded music file.
        ///
        inline Music music (const std::string &file_name) {
            return Music (Mix_LoadMUS (file_name.c_str ()), Mix_FreeMusic);
        }

        ///
        /// @brief Creates a new Renderer.
        ///
        /// @param[in] window The window where rendering is displayed.
        /// @param[in] index The index of the rendering driver to initialize.
        /// @param[in] flags Render flags.
        ///
        /// @return A renderer in a smart pointer.
        ///
        inline Renderer renderer (Window window, int index, Uint32 flags) {
            return Renderer (
                SDL_CreateRenderer (window.get (), index, flags),
                SDL_DestroyRenderer);
        }

        ///
        /// @brief Creates a new Sound from a file.
        ///
        /// @param[in] file_name The name of the sound file to load.
        ///
        /// @return The smart pointer to the loaded sound file.
        ///
        inline Sound sound (const std::string &file_name) {
            return Sound (Mix_LoadWAV (file_name.c_str ()), Mix_FreeChunk);
        }

        ///
        /// @brief Wraps a Surface in a smart pointer.
        ///
        /// @param[in] surface The SDL_Surface to wrap.
        ///
        /// @return The surface in an smart pointer.
        ///
        inline Surface surface (SDL_Surface *surface) {
            return Surface (surface, SDL_FreeSurface);
        }

        ///
        /// @brief Creates a new Texture from a Surface.
        ///
        /// @param renderer The renderer to use to create the Texture.
        /// @param surface The surface to get its data from.
        ///
        /// @return A new smart pointer to a Texture from the data of @p surface.
        ///
        inline Texture texture (SDL_Renderer *renderer, Surface surface) {
            return Texture (
                    SDL_CreateTextureFromSurface (renderer, surface.get ()),
                    SDL_DestroyTexture);
        }

        ///
        /// @brief Creates a new window.
        ///
        /// @param[in] title The Window's title.
        /// @param[in] x The X position of the Window.
        /// @param[in] y The Y position of the Window.
        /// @param[in] w The Window's width.
        /// @param[in] h The Window's height.
        /// @param[in] flags The creation flags.
        ///
        /// @return A new Window in a smart pointer.
        ///
        inline Window window (const std::string &title, int x, int y,
                int w, int h, Uint32 flags) {
            return Window (
                    SDL_CreateWindow (title.c_str (), x, y, w, h, flags),
                    SDL_DestroyWindow);
        }
    }
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SDL_HPP


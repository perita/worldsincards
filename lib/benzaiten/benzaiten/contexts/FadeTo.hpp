//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_CONTEXTS_FADE_TO_HPP)
#define GEISHA_STUDIOS_BENZAITEN_CONTEXTS_FADE_TO_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>
#include "../Context.hpp"

namespace benzaiten { namespace contexts {
    ///
    /// @class FadeTo
    /// @brief Fades from the current state to the next.
    ///
    class FadeTo: public Context {
        public:
            /// The pointer to a FadeTo instance.
            typedef boost::shared_ptr<FadeTo> Ptr;

            ///
            /// @brief Creates a new instance of the FadeTo context.
            ///
            /// @param[in] next The next context to siwtch to wehn done.
            /// @param[in] time The total delay in seconds to fade off and
            ///            fade in.
            ///
            /// @return The new instance of the FadeTo context.
            ///
            static Ptr New (const Context::Ptr &next, double time = 0.5) {
                return Ptr (new FadeTo (next, time));
            }

            virtual void render (SDL_Renderer &renderer, const SDL_Rect *clip) const;

        protected:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] next The next context to siwtch to wehn done.
            /// @param[in] time The total delay in seconds to fade off and
            ///            fade in.
            ///
            FadeTo (const Context::Ptr &next, double time = 0.5);

        private:
            ///
            /// @brief Swaps the drawing context to the next context.
            ///
            void swapDrawContext ();

            ///
            /// @brief Switches to the next context.
            ///
            void switchToNext ();

            /// The alpha value for the fade.
            double alpha;
            /// The context to draw.
            Context::Ptr draw;
            /// The next context to switch to.
            Context::Ptr next;
            /// The previous context.
            Context::Ptr prev;
    };
} }

#endif // !GEISHA_STUDIOS_BENZAITEN_CONTEXTS_FADE_TO_HPP

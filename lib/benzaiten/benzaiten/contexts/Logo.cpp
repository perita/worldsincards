//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "Logo.hpp"
#include <boost/bind.hpp>
#include "../Game.hpp"
#include "../Utils.hpp"
#include "../graphics/Texture.hpp"
#include "../tweeners/Sequence.hpp"
#include "../tweeners/Simple.hpp"
#include "../tweeners/easing/Linear.hpp"

using namespace benzaiten::contexts;

Logo::Logo (const std::string &logo_file, const Context::Ptr &next,
        double display_time, double fade_time):
    alpha (SDL_ALPHA_OPAQUE),
    next (next)
{
    Texture::Ptr logo = Texture::New (Game::resources ().texture (logo_file));
    logo->centerOrigin ();
    Sprite::Ptr sprite = this->addGraphic (logo,
            Game::screen ().half_width, Game::screen().height / 3);

    tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
    tweeners->add (tweener::Simple::New (this->alpha, SDL_ALPHA_TRANSPARENT,
                fade_time, easing::Linear::easeIn));
    tweeners->add (tweener::Simple::New (this->alpha, SDL_ALPHA_TRANSPARENT,
                SDL_ALPHA_OPAQUE, fade_time,
                easing::Linear::easeIn))->setDelay (display_time);
    tweeners->setFinishCallback (boost::bind (&Logo::switchToNext, this));
    this->addTweener (tweeners);
}

void Logo::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    Context::render (renderer, clip);

    SDL_Color color = Game::screen().color;
    SDL_SetRenderDrawColor (&renderer, color.r, color.g, color.b,
            this->alpha);
    SDL_BlendMode mode;
    SDL_GetRenderDrawBlendMode (&renderer, &mode);
    SDL_SetRenderDrawBlendMode (&renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderFillRect (&renderer, clip);
    SDL_SetRenderDrawBlendMode (&renderer, mode);
}

void Logo::switchToNext () {
    Game::switchTo (this->next);
}

//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "FadeTo.hpp"
#include <cassert>
#include <boost/bind.hpp>
#include "../Game.hpp"
#include "../tweeners/Sequence.hpp"
#include "../tweeners/Simple.hpp"
#include "../tweeners/easing/Linear.hpp"
#include "../tweeners/easing/None.hpp"

using namespace benzaiten::contexts;

FadeTo::FadeTo (const Context::Ptr &next, double time)
    : Context ()
    , alpha (SDL_ALPHA_TRANSPARENT)
    , draw ()
    , next (next)
    , prev (Game::activeContext ())
{
    assert (this->prev || this->next);

    if (this->prev) {
        this->draw = this->prev;
    } else {
        this->draw = this->next;
    }

    tweener::Sequence::Ptr tweeners = tweener::Sequence::New ();
    if (this->prev) {
        tweeners->add (tweener::Simple::New (this->alpha, SDL_ALPHA_TRANSPARENT,
                    SDL_ALPHA_OPAQUE, time / 2, easing::Linear::easeIn))
            ->setFinishCallback (boost::bind (&FadeTo::swapDrawContext, this));
    }
    if (this->next) {
        tweeners->add (tweener::Simple::New (this->alpha, SDL_ALPHA_OPAQUE,
                    SDL_ALPHA_TRANSPARENT, time / 2, easing::Linear::easeIn));
    }
    tweeners->setFinishCallback (boost::bind (&FadeTo::switchToNext, this));
    this->addTweener (tweeners);
}

void FadeTo::render (SDL_Renderer &renderer, const SDL_Rect *clip) const {
    if (this->draw) {
        this->draw->render (renderer, clip);

        SDL_Color color = Game::screen().color;
        SDL_SetRenderDrawColor (&renderer, color.r, color.g, color.b,
                this->alpha);
        SDL_BlendMode mode;
        SDL_GetRenderDrawBlendMode (&renderer, &mode);
        SDL_SetRenderDrawBlendMode (&renderer, SDL_BLENDMODE_BLEND);
        SDL_RenderFillRect (&renderer, clip);
        SDL_SetRenderDrawBlendMode (&renderer, mode);
    }
}

void FadeTo::swapDrawContext () {
    this->draw = this->next;
    // Make sure the graphic lists are updated for the next context to draw.
    if (this->draw) {
        this->draw->updateLists ();
    }
}

void FadeTo::switchToNext () {
    Game::switchTo (this->next);
}

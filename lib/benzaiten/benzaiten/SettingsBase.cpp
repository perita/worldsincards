//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "SettingsBase.hpp"
#include <cassert>

#if defined (WIN32)
#include "SettingsImplWin32.hpp"
#elif defined (APPLE)
#include "SettingsImplOSX.hpp"
#elif defined (UNIX)
#include "SettingsImplUnix.hpp"
#else
#error "This platform does not have a low level settings implementation."
#endif

using namespace benzaiten;

SettingsBase::SettingsBase (const std::string &appName):
#if defined (WIN32)
    impl_ (new SettingsImplWin32 (appName))
#elif defined (APPLE)
    impl_ (new SettingsImplOSX (appName))
#elif defined (UNIX)
    impl_ (new SettingsImplUnix (appName))
#endif
{
}

SettingsBase::~SettingsBase ()
{
    impl_.reset ();
}

int
SettingsBase::getInteger(const std::string &section, const std::string &name,
        int defaultValue) const
{
    assert (impl_);
    return impl_->getInteger(section, name, defaultValue);
}

std::string
SettingsBase::getString(const std::string &section, const std::string &name,
        const std::string &defaultValue) const
{
    assert (impl_);
    return impl_->getString(section, name, defaultValue);
}

void
SettingsBase::setInteger(const std::string &section, const std::string &name,
        int value)
{
    assert (impl_);
    impl_->setInteger(section, name, value);
}

void
SettingsBase::setString(const std::string &section, const std::string &name,
        const std::string &value)
{
    assert (impl_);
    impl_->setString(section, name, value);
}

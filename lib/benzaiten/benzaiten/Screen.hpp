//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SCREEN_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SCREEN_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL_stdinc.h>
#include <SDL_pixels.h>
#include <boost/noncopyable.hpp>
#include "SDL.hpp"
#include "Point2D.hpp"

namespace benzaiten {

    ///
    /// @class Screen
    /// @brief The representation of the game's screen.
    ///
    struct Screen: public boost::noncopyable {
        ///
        /// @brief The policy to fit the game's size to the screen's size.
        enum FillPolicy
        {
            FILL_NONE, /// Fill both height and width
            FILL_BOTH, /// Fill none of them
            FILL_HEIGHT, /// Fill only height, leave height alone
            FILL_WIDTH /// Fill only width, leave height alone
        };

        ///
        /// @brief Constructor.
        ///
        /// @param[in] width The intended screen's width.
        /// @param[in] height The intended screen's height.
        /// @param[in] title The title to give to the screen's window.
        /// @param[in] window_scale The scale to use for the actual
        ///            window size; If this is greater than 1, then the
        ///            window is made bigger than the intended screen's
        ///            size.  It can not be zero.
        /// @param[in] fill_policy The policy to use to fill the screen
        ///            to the window's size.
        ///
        Screen (int width, int height, const std::string &title,
                unsigned int window_scale, FillPolicy fill_policy);

        ///
        /// @brief Sets up the screen to start rendering.
        ///
        void beginRender ();

        ///
        /// @brief The rendering is complete.
        ///
        void endRender ();

        ///
        /// @brief Sets a new clip rectangle.
        ///
        /// @param[in] rect The new clip rectangle to set.
        ///
        void setClipRect (const SDL_Rect *rect);

        ///
        /// @brief Sets the default clip rectangle.
        ///
        void setDefaultClipRect ();

        /// The screen's clip rectangle.
        const SDL_Rect *clip;
        /// The screen's color.
        SDL_Color color;
        /// Half the screen's height.
        int half_height;
        /// Hald the screen's width.
        int half_width;
        /// The screen's height.
        int height;
        /// The display renderer.
        SDL::Renderer renderer;
        /// The screen's translation.
        Point2D<double> translation;
        /// The screen's scale.
        int scale;
        /// The screen's width.
        int width;
        /// The window to show the game.
        SDL::Window window;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SCREEN_HPP

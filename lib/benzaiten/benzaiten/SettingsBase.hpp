//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010, 2011, 2012 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SETTINGS_BASE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SETTINGS_BASE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <stdexcept>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace benzaiten
{
    ///
    /// @class SettingsBase
    /// @brief Manages the game's settings.
    ///
    class SettingsBase: public boost::noncopyable
    {
        public:
            ///
            /// @class Impl
            /// @brief The interface of low level operations for SettingsBase.
            ///
            class Impl
            {
                public:
                    ///
                    /// @brief Destructor.
                    ///
                    virtual ~Impl () {}

                    ///
                    /// @brief Gets an integer option.
                    ///
                    /// @param[in] section The section where the value is
                    ///            located at.
                    /// @param[in] name The name of the value to get.
                    /// @param[in] defaultValue The value to return in case
                    ///            @p name doesn't exist in @p section.
                    /// @return The value of the option named @p name in
                    ///         @p section.
                    ///
                    virtual int getInteger(const std::string &section,
                            const std::string &name, int defaultValue) const = 0;

                    ///
                    /// @brief Gets an string option.
                    ///
                    /// @param[in] section The section where the value is
                    ///            located at.
                    /// @param[in] name The name of the value to get.
                    /// @param[in] defaultValue The value to return in case
                    ///            @p name doesn't exist in @p section.
                    /// @return The value of the option named @p name in
                    ///         @p section.
                    ///
                    virtual std::string getString(const std::string &section,
                            const std::string &name,
                            const std::string &defaultValue) const = 0;

                    ///
                    /// @brief Sets an integer option.
                    ///
                    /// @param[in] section The section where to write the
                    ///            value to.
                    /// @param[in] name The name to use to save the value with.
                    /// @param[in] value The value to save as @p name in
                    ///            @p section.
                    ///
                    virtual void setInteger(const std::string &section,
                            const std::string &name, int value) = 0;

                    ///
                    /// @brief Sets an string option.
                    ///
                    /// @param[in] section The section where to write the
                    ///            value to.
                    /// @param[in] name The name to use to save the value with.
                    /// @param[in] value The value to save as @p name in
                    ///            @p section.
                    ///
                    virtual void setString(const std::string &section,
                            const std::string &name,
                            const std::string &value) = 0;
            };


            ///
            /// @brief Constructor.
            ///
            /// @param[in] appName The name of the application.
            ///
            SettingsBase(const std::string &appName);

            ///
            /// @brief Destructor.
            ///
            virtual ~SettingsBase ();

        protected:
            ///
            /// @brief Gets an integer option.
            ///
            /// @param[in] section The section where the value is located at.
            /// @param[in] name The name of the value to get.
            /// @param[in] defaultValue The value to return in case @p name
            ///            doesn't exist in @p section.
            /// @return The value of the option named @p name in
            ///         @p section.
            ///
            int getInteger (const std::string &section,
                    const std::string &name, int defaultValue) const;

            ///
            /// @brief Gets an string option.
            ///
            /// @param[in] section The section where the value is
            ///            located at.
            /// @param[in] name The name of the value to get.
            /// @param[in] defaultValue The value to return in case
            ///            @p name doesn't exist in @p section.
            /// @return The value of the option named @p name in
            ///         @p section.
            ///
            virtual std::string getString(const std::string &section,
                    const std::string &name,
                    const std::string &defaultValue) const;

            ///
            /// @brief Sets an integer option.
            ///
            /// @param[in] section The section where to write the value to.
            /// @param[in] name The name to use to save the value with.
            /// @param[in] value The value to save as @p name in @p section.
            ///
            void setInteger (const std::string &section,
                    const std::string &name, int value);

            ///
            /// @brief Sets an string option.
            ///
            /// @param[in] section The section where to write the
            ///            value to.
            /// @param[in] name The name to use to save the value with.
            /// @param[in] value The value to save as @p name in
            ///            @p section.
            ///
            virtual void setString(const std::string &section,
                    const std::string &name, const std::string &value);

        private:
            /// The pointer to the low level implementation.
            boost::shared_ptr<Impl> impl_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SETTINGS_BASE_HPP

Ludum Dare 23
=============
Jordi “summaky” Fita <jfita@geishastudios.com>
April 2012

“Worlds in Cards” is my entry for the 23rd Ludum Dare, which had “Tiny Worlds”
as its theme.

In “Worlds in Cards” there are a certain amount of cards on the screen where
each card is a very tiny world.  In each world there can be the player,
sapphires, or other critters that wander around.

Fortunately, these worlds can be put side by side for form “bigger” worlds.
Characters can move freely between worlds but only if the landscape matches!
That is, if one world has a ladder that goes down, the world at the bottom
better have a ladder up, or the characters can't pass.  Another peculiarity of
these world is that they can be swapped one by another.  However, this is only
possible if there is nothing and no one inside that world.

Your job is, then, to swap empty world to try to make a path that leads the
player from its starting position until all the sapphires scattered around the
level.  Beware that there are some worlds that are perilous and you might be
not alone in this strange universe.

The screen size is also limited in size.  I want to think that it is done on
purpose as to fit the theme, but actually is because this way looks better on
my Android phone :-)

Cheers!
